#ifndef WHATSAPPTRACKER_SYNCRESULT_H
#define WHATSAPPTRACKER_SYNCRESULT_H

#include <QStringList>
#include <QMap>

namespace whatsapptracker {

class SyncResult
{
public:
	SyncResult(const QString& index, const QString& syncId, QMap<QString, QString> existing, QStringList nonExisting)
	{
		this->_index = index;
		this->_syncId = syncId;
		this->_existing = existing;
		this->_nonExisting = nonExisting;
	}

	QString index() const { return this->_index;	}
	QString syncId() const { return this->_syncId; }
	QMap<QString, QString> existing() const { return this->_existing; }
	QStringList nonExisting() const { return this->_nonExisting; }

private:
	QString _index;
	QString _syncId;
	QMap<QString, QString> _existing;
	QStringList _nonExisting;
};
}
#endif