#include <QCoreApplication>
#include <QByteArray>
#include "controllers/whatsappcontroller.h"
#include "controllers/dbcontroller.h"
#include "models/user.h"
#include "whatsappcontrollertester.h"

namespace whatsapptracker {
class TempClass : public whatsapptracker::GetUsersCallback, public whatsapptracker::UpdateUserCallback
{
public:
    TempClass(DbController* dbController) {
        this->dbController = dbController;
    }

    void onGetUsers(const QList<User>& users)
    {
        if (users.length() > 0) {
            User user = users.at(0);
            user.setPhoneNumber("6590696533");
            this->dbController->updateUser(user, this);

        }
    }

    void onUpdateUser(const bool status) {
        qDebug("here");
    }

private:
    DbController* dbController;
};
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    /*whatsapptracker::DbController databaseController;
    databaseController.setup("ds047752.mongolab.com:47752", "whatsapptracker", "whatsapptrackeruser", "2ry2wd2", whatsapptracker::DbController::mongo);
    databaseController.start();

    while (!databaseController.isRunning()) {
       
    }
    whatsapptracker::TempClass temp( &databaseController ) ;
    databaseController.getUsers(&temp);*/
	//whatsapptracker::WhatsAppController controller;
	//controller.connect();
    whatsapptracker::WhatsAppControllerTester tester;
    tester.runAllTests();
    
    return a.exec();
}
