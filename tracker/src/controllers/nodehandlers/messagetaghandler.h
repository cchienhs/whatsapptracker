#ifndef WHATSAPPTRACKER_MESSAGETAGHANDLER_H
#define WHATSAPPTRACKER_MESSAGETAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class MessageTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif