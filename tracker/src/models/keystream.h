#ifndef KEYSTREAM_H
#define KEYSTREAM_H

#include <QByteArray>
#include <QString>
#include <QList>
#include <QCryptographicHash>

namespace whatsapptracker{

struct KeyStreamPrivate;
class KeyStream
{
public:
	KeyStream(const QByteArray& key, const QByteArray& macKey);
	KeyStream(const KeyStream &obj);  // copy constructor
	~KeyStream();

	QByteArray decodeMessage(const QByteArray& buffer, unsigned int macOffset, unsigned int offset, unsigned int length);
	QByteArray KeyStream::encodeMessage(const QByteArray& buffer, unsigned int macOffset, unsigned int offset, unsigned int length);
	
	static QList<QByteArray> generateKeys(const QString& password, const QByteArray& nonce);

private:
	QByteArray computeMac(const QByteArray& buffer, unsigned int offset, unsigned int length);
	KeyStreamPrivate *d;
};
}
#endif // KEYSTREAM_H

