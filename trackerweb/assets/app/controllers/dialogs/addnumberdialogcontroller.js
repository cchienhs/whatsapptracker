
angular.module('whatsAppTrackerApp').controller('addNumberDialogController', function ($scope, $modalInstance)
{

    $scope.ok = function () {
        $modalInstance.close($scope.newNumber);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});