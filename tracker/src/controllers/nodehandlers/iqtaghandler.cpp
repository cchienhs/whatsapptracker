#include <QMap>
#include <QStringList>
#include "../whatsappcontroller.h"
#include "../../constants.h"
#include "../../models/protocolnode.h"
#include "../../models/syncresult.h"
#include "../../helpers/whatsapphelper.h"
#include "iqtaghandler.h"

namespace whatsapptracker {

void IqTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (node->getAttribute("type").compare("get") == 0
		&& node->getAttribute("xmlns").compare("urn:xmpp:ping") == 0) {
		controller->onPing(controller->phoneNumber(), node->getAttribute("id"));
		controller->sendPong(node->getAttribute("id"));
	}

	if (node->getChild("sync") != NULL) {

		//sync result
		ProtocolNode* sync = node->getChild("sync");
		ProtocolNode* existing = sync->getChild("in");
		ProtocolNode* nonexisting = sync->getChild("out");

		//process existing first
		QMap<QString, QString> existingUsers;
		if (existing != NULL) {
			foreach(ProtocolNode* child, existing->getChildren()) {
				existingUsers[child->getData()] = child->getAttribute("jid");
			}
		}

		//now process failed numbers
		QStringList failedNumbers;
		if (nonexisting != NULL) {
			foreach(ProtocolNode* child, nonexisting->getChildren()) {
				failedNumbers.append(child->getData().replace("+", ""));
			}
		}

		QString index = sync->getAttribute("index");
		SyncResult *result = new SyncResult(index, sync->getAttribute("sid"), existingUsers, failedNumbers);
		controller->onGetSyncResult(result);
		delete result;
	}

	if (node->getAttribute("type").compare("result") == 0) {
		QMap<QString, QString> nodeId = controller->nodeId();

		if (node->getChild("query") != NULL) {
			QMap<QString, QString> nodeId = controller->nodeId();
			if (nodeId.contains("privacy") && (nodeId["privacy"].compare(node->getAttribute("id")) == 0)) {
				QStringList blockedJids;
				ProtocolNode* listChild = node->getChild(0)->getChild(0);
				foreach(ProtocolNode* child, listChild->getChildren()) {
					blockedJids.append(child->getAttribute("value"));
				}

				controller->onGetPrivacyBlockedList(controller->phoneNumber(), blockedJids);
			}
			controller->onGetLastSeen(controller->phoneNumber(),
				WhatsAppHelper::convertJidToPhoneNumber( node->getAttribute("from") ),
				node->getAttribute("id"),
				node->getChild(0)->getAttribute("seconds"));

			controller->messageQueue().append(node);
		}
		if (node->getChild("props") != NULL) {
			//server properties
			QMap<QString, QString> props;
			foreach(ProtocolNode* child, node->getChild(0)->getChildren()) {
				props[child->getAttribute("name")] = child->getAttribute("value");
			}
			controller->onGetServerProperties(controller->phoneNumber(),
				node->getChild(0)->getAttribute("version"),
				props);
		}
		if (node->getChild("picture") != NULL) {
            QString from = WhatsAppHelper::convertJidToPhoneNumber( node->getAttribute("from") );
			controller->onGetProfilePicture( controller->phoneNumber(),
				from,
				node->getChild("picture")->getAttribute("type"),
				node->getChild("picture")->getData());
		}
		if (node->getChild("media") != NULL || node->getChild("duplicate") != NULL) {
			controller->processUploadResponse(node);
		}
		if ( node->getAttribute("from").indexOf( Constants::WHATSAPP_GROUP_SERVER ) >= 0)  {
			//There are multiple types of Group reponses. Also a valid group response can have NO children.
			//Events fired depend on text in the ID field.
			QList<QMap<QString, QString>> groupList;
			QList<ProtocolNode*> groupNodes;
			if (node->getChild(0) != NULL && node->getChild(0)->getChildren().length()>0) {
				foreach( ProtocolNode* child, node->getChild(0)->getChildren() ) {
					groupList.append( child->getAttributes());
					groupNodes.append( child );
				}
			}

			if ( nodeId.contains("groupcreate") && (nodeId["groupcreate"].compare(node->getAttribute("id")) == 0)) {
				controller->setGroupId( node->getChild(0)->getAttribute("id"));
				controller->onGroupsChatCreate(controller->phoneNumber(), controller->groupId());
			}

			if ( nodeId.contains("leavegroup") && (nodeId["leavegroup"].compare(node->getAttribute("id")) == 0)) {
				controller->setGroupId( node->getChild(0)->getChild(0)->getAttribute("id"));
				controller->onGroupsChatEnd(controller->phoneNumber(), controller->groupId());
			}

			if ( nodeId.contains("getgroups") && (nodeId["getgroups"].compare(node->getAttribute("id"))==0)) {
				controller->onGetGroups( controller->phoneNumber(),groupList);

				//getGroups returns a array of nodes which are exactly the same as from getGroupV2Info
				//so lets call this event, we have all data at hand, no need to call getGroupV2Info for every
				//group we are interested
				foreach( ProtocolNode* groupNode, groupNodes ) {
					controller->handleGroupV2InfoResponse(groupNode, true);
				}
			}

			if ( nodeId.contains("get_groupv2_info") && (nodeId["get_groupv2_info"].compare(node->getAttribute("id"))==0 )) {
				ProtocolNode* groupChild = node->getChild(0);
				if (groupChild != NULL) {
					controller->handleGroupV2InfoResponse(groupChild);
				}
			}
		}
		if (nodeId.contains("get_lists") && (nodeId["get_lists"].compare(node->getAttribute("id")) == 0 )) {
			QMap<QString,QMap<QString,QString>> broadcastLists;
			if (node->getChild(0) != NULL) {
				QList<ProtocolNode*> childArray = node->getChildren();
				foreach( ProtocolNode* list, childArray ) {
					if (list->getChildren().length() > 0) {
						foreach( ProtocolNode* sublist, list->getChildren() ) {
							QString id = sublist->getAttribute("id");
							QString name = sublist->getAttribute("name");
							broadcastLists[id]["name"] = name;
							QStringList recipents;
							foreach(ProtocolNode* recipent, sublist->getChildren()) {
								recipents.append( recipent->getAttribute("jid") );
							}
							broadcastLists[id]["recipients"] = recipents.join(",");
						}
					}
				}
			}
			controller->onGetBroadcastLists( controller->phoneNumber(), broadcastLists );
		}
		if (node->getChild("pricing") != NULL) {
			controller->onGetServicePricing(
				controller->phoneNumber(),
				node->getChild(0)->getAttribute("price"),
				node->getChild(0)->getAttribute("cost"),
				node->getChild(0)->getAttribute("currency"),
				node->getChild(0)->getAttribute("expiration"));
		}
		if (node->getChild("extend") != NULL) {
			controller->onGetExtendAccount(
				controller->phoneNumber(),
				node->getChild("account")->getAttribute("kind"),
				node->getChild("account")->getAttribute("status"),
				node->getChild("account")->getAttribute("creation"),
				node->getChild("account")->getAttribute("expiration"));
		}
		if (node->getChild("normalize") != NULL) {
			controller->onGetNormalizedJid(
				controller->phoneNumber(),
				node->getChild(0)->getAttribute("result"));
		}
		if (node->getChild("status") != NULL) {
			ProtocolNode* child = node->getChild("status");
			foreach( ProtocolNode* status, child->getChildren() ) {
				controller->onGetStatus(
					controller->phoneNumber(),
					WhatsAppHelper::convertJidToPhoneNumber( status->getAttribute("jid") ),
					"requested",
					node->getAttribute("id"),
					status->getAttribute("t"),
					status->getData());
			}
		}
	}
	if (node->getAttribute("type").compare("error") == 0) {
		controller->onGetError(
			controller->phoneNumber(),
			node->getAttribute("from"),
			node->getAttribute("id"),
			node->getChild(0));
	}
}
}