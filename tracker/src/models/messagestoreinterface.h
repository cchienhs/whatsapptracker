#ifndef WHATSAPPTRACKER_MESSAGESTOREINTERFACE_H
#define WHATSAPPTRACKER_MESSAGESTOREINTERFACE_H

#include <QString>

namespace whatsapptracker {

class MessageStoreInterface
{
public:
	virtual void saveMessage( const QString& from, const QString& to, const QString& txt, const QString& id, const QString& t)= 0;
};
}
#endif