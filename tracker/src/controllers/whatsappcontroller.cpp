#include <QFile>
#include <QTcpSocket>
#include <QDateTime>
#include <QQueue>
#include "whatsappcontroller.h"
#include "../helpers/cryptohelper.h"
#include "../helpers/mathhelper.h"
#include "../helpers/filesystemhelper.h"
#include "../helpers/loghelper.h"
#include "../models/protocolnode.h"
#include "../models/bintreenodereader.h"
#include "../models/bintreenodewriter.h"
#include "../models/keystream.h"
#include "../models/messagestoreinterface.h"
#include "../models/syncresult.h"
#include "nodehandlers/successtaghandler.h"
#include "nodehandlers/acktaghandler.h"
#include "nodehandlers/receipttaghandler.h"
#include "nodehandlers/presencetaghandler.h"
#include "nodehandlers/chatstatetaghandler.h"
#include "nodehandlers/messagetaghandler.h"
#include "nodehandlers/iqtaghandler.h"
#include "nodehandlers/notificationtaghandler.h"
#include "nodehandlers/calltaghandler.h"
#include "nodehandlers/ibtaghandler.h"
#include "../constants.h"

namespace whatsapptracker {

struct WhatsAppControllerPrivate
{
    QString challengeFilename;
    QByteArray challengeData;
    QByteArray unprocessedData;
    QTcpSocket socket;
    QString phoneNumber;
    QString password;
    QString name;
    QString loginStatus;
	QString serverReceivedId;
	QString lastId;
	QString groupId;
	unsigned int messageCounter = 0;
	QQueue<ProtocolNode*> messageQueue;
	QQueue<ProtocolNode*> outQueue;
	bool debug;

	QMap<QString, QString> nodeId;
	QMap<QString,ProtocolNode*> mediaQueue;
	BinTreeNodeReader* reader = NULL;
	BinTreeNodeWriter* writer = NULL;
	KeyStream* inputKey = NULL;
	KeyStream* outputKey = NULL;
	MessageStoreInterface *messageStore = NULL;
	QDateTime loginTime;
};


WhatsAppController::WhatsAppController( const QString& number, const QString& secret, const QString& nickname, const bool debug, QObject* parent )
	: QObject( parent ),
	  d( new WhatsAppControllerPrivate )
{
	d->writer = new BinTreeNodeWriter();
	d->reader = new BinTreeNodeReader();
	d->debug = debug;
    d->phoneNumber = number;

	d->challengeFilename = QString("%1/nextChallenge.%2.dat").arg(Constants::DATA_FOLDER, number);
	FileSystemHelper::createFile(d->challengeFilename);

    d->password = secret;
    d->name = nickname;
    d->loginStatus = Constants::DISCONNECTED_STATUS;
   
}

/**
* Send a pong to the WhatsApp server. I'm alive!
*
* @param string $msgid The id of the message.
*/
void WhatsAppController::sendPong(const QString& msgid)
{
	QMap<QString, QString> attributes;
	attributes["to"] = Constants::WHATSAPP_SERVER;
	attributes["id"] = msgid;
	attributes["type"] = "result";

	ProtocolNode* messageNode = new ProtocolNode("iq", attributes, QList<ProtocolNode*>(), QByteArray());
	this->sendNode(messageNode);

	emit onSendPong( d->phoneNumber, msgid );
}

QString WhatsAppController::groupId() const
{
	return d->groupId;
}

void WhatsAppController::setGroupId(const QString& input)
{
	d->groupId = input;
}

QMap<QString, QString>& WhatsAppController::nodeId() const
{
	return d->nodeId;
}

QString WhatsAppController::phoneNumber() const
{
	return d->phoneNumber;
}

QString WhatsAppController::challengeFilename() const
{
	return d->challengeFilename;
}

QString WhatsAppController::lastId() const
{
	return d->lastId;
}

MessageStoreInterface* WhatsAppController::messageStore() const
{
	return d->messageStore;
}

QQueue<ProtocolNode*>& WhatsAppController::messageQueue() const
{
	return d->messageQueue;
}

BinTreeNodeWriter* WhatsAppController::writer() const
{
	return d->writer;
}

KeyStream* WhatsAppController::outputKey() const
{
	return d->outputKey;
}

bool WhatsAppController::isConnected()
{
	return (d->socket.state() == QTcpSocket::ConnectedState);
}

void WhatsAppController::connect() 
{
	QObject::connect(&(d->socket), SIGNAL(connected()), this, SLOT(onConnected()));
	QObject::connect(&(d->socket), SIGNAL(disconnected()), this, SLOT(onDisconnected()));
	QObject::connect(&(d->socket), SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
	QObject::connect(&(d->socket), SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	QObject::connect(&(d->socket), SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
	d->socket.connectToHost("e" + QString::number(MathHelper::randInt(1, 16)) + ".whatsapp.net", Constants::PORT);	
}

void WhatsAppController::setLoginStatus(const QString& input)
{
	d->loginStatus = input;
}

QString WhatsAppController::loginStatus() const
{
    return d->loginStatus;
}

QByteArray WhatsAppController::retrieveChallengeData()
{
	return FileSystemHelper::getContent(challengeFilename());

}

bool WhatsAppController::isLoggedIn()
{
	return (this->isConnected() && !d->loginStatus.isEmpty() && (d->loginStatus.compare(Constants::CONNECTED_STATUS) == 0));
}

void WhatsAppController::login()
{
	QByteArray challengeData = retrieveChallengeData();
	if (challengeData.length())
		d->challengeData = challengeData;

	if (isLoggedIn())
		return;

	d->writer->resetKey();
	d->reader->resetKey();
	QString resource = Constants::WHATSAPP_DEVICE + '-' + Constants::WHATSAPP_VER + '-' + QString::number(Constants::PORT);
	QByteArray data = d->writer->startStream( Constants::WHATSAPP_SERVER, resource ).toLatin1();
	ProtocolNode* feat = this->createFeaturesNode();
	ProtocolNode* auth = this->createAuthNode();
	
	this->sendData(data);
	this->sendNode(feat);
	this->sendNode(auth);

	//if ($this->loginStatus === Constants::DISCONNECTED_STATUS) {
	//    throw new LoginFailureException();
	//}

	//$this->eventManager()->fire("onLogin",
	//   array(
	//      $this->phoneNumber
	// ));
	//$this->sendAvailableForChat();
	//$this->loginTime = time();
}

QByteArray WhatsAppController::readStanza()
{
	QByteArray buff;
	unsigned int toRead;

	if (d->socket.isValid()) {
        d->unprocessedData += d->socket.readAll();
        if (d->unprocessedData.length() >= 3) {

            unsigned int treeLength = (((unsigned char)d->unprocessedData.at(0) & 0x0F)) << 16;
            treeLength |= (unsigned char)d->unprocessedData.at(1) << 8;
            treeLength |= (unsigned char)d->unprocessedData.at(2);

            if (d->unprocessedData.length() >= (treeLength + 3)) {
                buff = d->unprocessedData.mid(0, treeLength + 3);
                d->unprocessedData = d->unprocessedData.remove(0, treeLength + 3);
            }
        }
	}

	return buff;
}

void WhatsAppController::sendAck( ProtocolNode* node, const QString& className )
{
	QString from = node->getAttribute("from");
	QString to = node->getAttribute("to");
	QString participant = node->getAttribute("participant");
	QString id = node->getAttribute("id");
	QString type = node->getAttribute("type");

	QMap<QString, QString> attributes;
	if (!to.isEmpty())
		attributes["from"] = to;
	if (!participant.isEmpty())
		attributes["participant"] = participant;
	attributes["to"] = from;
	attributes["class"] = className;
	attributes["id"] = id;
	if (!type.isEmpty())
		attributes["type"] = type;

	ProtocolNode* ack = new ProtocolNode("ack", attributes, QList<ProtocolNode*>(), QByteArray());
	this->sendNode(ack);

}

void WhatsAppController::processChallenge(const ProtocolNode* node )
{
	//176 109 144 254 131 15 44 125 71 255 182 86 245 122 31 108 145 57 110 155
	QByteArray data = node->getData();
	QString output;
	for (int i = 0; i < data.length(); ++i)
		output += QString::number((unsigned char)data[i]) + " ";
	qDebug("%s", output.toLatin1().constData());
	d->challengeData = data;
}

void WhatsAppController::sendNextMessage()
{
	if (d->outQueue.length() > 0 ) {
		ProtocolNode* msgnode = d->outQueue.takeFirst();
		msgnode->refreshTimes();
		d->lastId = msgnode->getAttribute("id");
		this->sendNode(msgnode);
	}
	else {
		d->lastId.clear();
	}
}

QString WhatsAppController::createMsgId( const QString& prefix )
{
	unsigned int msgid = d->messageCounter;
	d->messageCounter++;

	return prefix + "-" + QString::number( d->loginTime.toTime_t() ) + "-" + QString::number(msgid);
}

/**
* Clears the "dirty" status on your account
*
* @param  array $categories
*/
void WhatsAppController::sendClearDirty(const QStringList& categories)
{
	QString msgId = this->createMsgId();

	QList<ProtocolNode*> catnodes;
	foreach( QString category, categories ) {
		QMap<QString, QString> attributes;
		attributes["type"] = category;
		ProtocolNode* catnode = new ProtocolNode("clean", attributes, QList<ProtocolNode*>(), QByteArray());
		catnodes.append( catnode );
	}

	QMap<QString, QString> attributes;
	attributes["id"] = msgId;
	attributes["type"] = "set";
	attributes["to"] = Constants::WHATSAPP_SERVER;
	attributes["xmlns"] = "urn:xmpp:whatsapp:dirty";

	ProtocolNode* node = new ProtocolNode("iq", attributes, catnodes, QByteArray());
	this->sendNode(node);
}

/**
* Tell the server we received the message.
*
* @param ProtocolNode $node The ProtocolTreeNode that contains the message.
* @param string       $type
* @param string       $participant
* @param string       $callId
*/
void WhatsAppController::sendReceipt( ProtocolNode* node, const QString& type, const QString& participant, const QString& callId)
{
	QMap<QString, QString> messageHash;
	ProtocolNode *messageNode = NULL;

	if (type.compare("read") == 0) {
		messageHash["type"] = type;
	}
	if (!participant.isEmpty()) {
		messageHash["participant"] = participant;
	}
	messageHash["to"] = node->getAttribute("from");
	messageHash["id"] = node->getAttribute("id");

	if (!callId.isEmpty())
	{
		QMap<QString, QString> attributes;
		attributes["call-id"] = callId;
		ProtocolNode* offerNode = new ProtocolNode("offer", attributes, QList<ProtocolNode*>(), QByteArray());
		QList<ProtocolNode*> list;
		list << offerNode;
		messageNode = new ProtocolNode("receipt", messageHash, list, QByteArray());
	}
	else
	{
		messageNode = new ProtocolNode("receipt", messageHash, QList<ProtocolNode*>(), QByteArray());
	}
	this->sendNode(messageNode);
	emit onSendMessageReceived( 
			d->phoneNumber,
			node->getAttribute("id"),
			node->getAttribute("from"),
			type);
}

/**
* Process media upload response
*
* @param ProtocolNode $node Message node
* @return bool
*/
bool WhatsAppController::processUploadResponse( ProtocolNode* node )
{
	QString id = node->getAttribute("id");
	ProtocolNode* messageNode = d->mediaQueue[id];
	if (messageNode == NULL) {
		//message not found, can't send!
		emit onMediaUploadFailed(d->phoneNumber,
			id,
			node,
			messageNode,
			"Message node not found in queue");
		return false;
	}

	ProtocolNode* duplicate = node->getChild("duplicate");
	QString url, filesize, filehash, filetype, filename;
	QStringList exploded;

	if (duplicate != NULL) {
		//file already on whatsapp servers
		url = duplicate->getAttribute("url");
		filesize = duplicate->getAttribute("size");
		filehash = duplicate->getAttribute("filehash");
		filetype = duplicate->getAttribute("type");
		exploded = url.split("/");
		filename = exploded.last();
	}
	else { //outstanding
		/*//upload new file
		$json = WhatsMediaUploader::pushFile($node, $messageNode, $this->mediaFileInfo, $this->phoneNumber);

		if (!$json) {
		//failed upload
		$this->eventManager()->fire("onMediaUploadFailed",
		array(
		$this->phoneNumber,
		$id,
		$node,
		$messageNode,
		"Failed to push file to server"
		));
		return false;
		}

		$url = $json->url;
		$filesize = $json->size;
		//          $mimetype = $json->mimetype;
		$filehash = $json->filehash;
		$filetype = $json->type;
		//          $width = $json->width;
		//          $height = $json->height;
		$filename = $json->name;
		}

		QMap<QString, QString> mediaAttribs;
		mediaAttribs["type"] = filetype;
		mediaAttribs["url"] = url;
		mediaAttribs["encoding"] = "raw";
		mediaAttribs["file"] = filename;
		mediaAttribs["size"] = filesize;
		if (!(d->mediaQueue[id]->getAttribute("caption").isEmpty())) {
		mediaAttribs["caption"] = d->mediaQueue[id]->getAttribute("caption");
		}

		QString filepath = d->mediaQueue[id]->getAttribute("filePath");
		QString to = d->mediaQueue[id]->getAttribute("to");

		if (filetype.compare("image") == 0) {
		$caption = $this->mediaQueue[$id]['caption'];
		$icon = createIcon($filepath);
		}
		else if (filetype.compare("video") == 0) {
		$caption = $this->mediaQueue[$id]['caption'];
		$icon = createVideoIcon($filepath);
		}
		else {
		$caption = '';
		$icon = '';
		break;
		}
		//Retrieve Message ID
		$message_id = $messageNode['message_id'];

		$mediaNode = new ProtocolNode("media", $mediaAttribs, null, $icon);
		if (is_array($to)) {
		$this->sendBroadcast($to, $mediaNode, "media");
		}
		else {
		$this->sendMessageNode($to, $mediaNode, $message_id);
		}
		$this->eventManager()->fire("onMediaMessageSent",
		array(
		$this->phoneNumber,
		$to,
		$id,
		$filetype,
		$url,
		$filename,
		$filesize,
		$filehash,
		$caption,
		$icon
		));*/
	}
	return true;
}

void WhatsAppController::setLoginTime(const QDateTime& loginTime)
{
    d->loginTime = loginTime;
}

void WhatsAppController::processInboundDataNode( ProtocolNode* node, const bool autoReceipt, const QString& type ) 
{
	if (node == NULL)
		return;

	qDebug(node->getTag().toLatin1());
	d->serverReceivedId = node->getAttribute("id");

	if (node->getTag().compare("challenge") == 0) {
		this->processChallenge(node);
		ProtocolNode* data = this->createAuthResponseNode();
		this->sendNode(data);
		d->reader->setKey(d->inputKey);
		d->writer->setKey(d->outputKey);

		//this->sendAvailableForChat();
		//$this->loginTime = time();
	} 
	else if(node->getTag().compare("failure") == 0) {
		d->loginStatus = Constants::DISCONNECTED_STATUS;
		//emit onLoginFailure(d->phoneNumber, node->getChild(0)->getTag());
	} 
	else if(node->getTag().compare("success") == 0) {
		SuccessTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	} 
	else if( (node->getTag().compare("ack") == 0) && (node->getAttribute("class").compare("message") == 0 ) ) {
		AckTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	} 
	else if(node->getTag().compare("receipt") == 0) {
		ReceiptTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("message") == 0 ) {
		MessageTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("presence") == 0) {
		PresenceTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("chatstate") == 0) {
		ChatStateTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("iq") == 0) {
		IqTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("notification") == 0) {
		NotificationTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}
	else if (node->getTag().compare("call") == 0) {
		CallTagHandler handler;

	}
	else if (node->getTag().compare("ib") == 0) {
		IbTagHandler handler;
		handler.handle(this, node, autoReceipt, type);
	}

	ProtocolNode* children = node->getChild(0);
	if (node->getTag().compare("stream:error") == 0 && children != NULL && node->getChild(0)->getTag().compare("system-shutdown")==0)
	{
		emit onStreamError( node->getChild(0)->getTag() );
	}

	if (node->getTag().compare("stream:error") == 0) {
		emit onStreamError(node->getChild(0)->getTag());
		this->disconnect();
	}
}

void WhatsAppController::processInboundData(const QByteArray& data, const bool autoReceipt, const QString& type)
{
	ProtocolNode* node = d->reader->nextTree( data );
	if (node != NULL) {
		this->processInboundDataNode(node, autoReceipt, type);
	}

}

bool WhatsAppController::pollMessage( const bool autoReceipt, const QString& type )
{
	if (!this->isConnected()) {
		LogHelper::log( "Connection Closed!" );
		return false;
	}

	QByteArray stanza = this->readStanza();
    qDebug("Length=%d\n", stanza.length());
	// Something to read
	if (stanza.length() > 0) {
		this->processInboundData(stanza, autoReceipt, type);
		return true;
	}
	
	return false;
}

void WhatsAppController::sendData(const QByteArray& data)
{
	/*QString output;
	for (int i = 0; i < data.length(); ++i)
		output += QString::number((unsigned char)data[i]) + " ";
	qDebug("Send data: %s", output.toLatin1().constData());*/
	d->socket.write(data);
	d->socket.flush();
}

void WhatsAppController::sendNode( ProtocolNode* node, bool encrypt )
{
	QByteArray data = d->writer->write(node, encrypt).toLatin1();
	this->sendData( data );
}

ProtocolNode* WhatsAppController::createFeaturesNode()
{
	ProtocolNode* readReceipts = new ProtocolNode("readreceipts", QMap<QString, QString>(), QList<ProtocolNode*>(), QByteArray());
	ProtocolNode* groupsv2 = new ProtocolNode("groups_v2", QMap<QString, QString>(), QList<ProtocolNode*>(), QByteArray());
	ProtocolNode* privacy = new ProtocolNode("privacy", QMap<QString, QString>(), QList<ProtocolNode*>(), QByteArray());
	ProtocolNode* presencev2 = new ProtocolNode("presence", QMap<QString, QString>(), QList<ProtocolNode*>(), QByteArray());
	
	QList<ProtocolNode*> list;
	list << readReceipts << groupsv2 << privacy << presencev2;
	
	ProtocolNode* parent = new ProtocolNode("stream:features", QMap<QString, QString>(), list, QByteArray());
	return parent;
}

QByteArray WhatsAppController::authenticate()
{
    QList<QByteArray> keys = KeyStream::generateKeys(d->password, d->challengeData );
	d->inputKey = new KeyStream(keys[2], keys[3]);
	d->outputKey = new KeyStream(keys[0], keys[1]);
	QByteArray input;
	input.append(QChar(0));
	input.append(QChar(0));
	input.append(QChar(0));
	input.append(QChar(0));

	input += d->phoneNumber.toLatin1() + d->challengeData;

	QByteArray response = d->outputKey->encodeMessage( input, 0, 4, input.length() - 4);
	return response;
}

QByteArray WhatsAppController::createAuthBlob()
{
	QByteArray ret;
	if (d->challengeData.length() > 0) {
		QByteArray key = CryptoHelper::pbkdf2(QCryptographicHash::Sha1, d->password, QString::fromLatin1(d->challengeData), 16, 20, true);
		d->inputKey = new KeyStream(QByteArray(1,key[2]), QByteArray(1, key[3]));
		d->outputKey = new KeyStream(QByteArray(1,key[0]), QByteArray(1, key[1]));
		d->reader->setKey(d->inputKey);
		QByteArray input;
		input.append(QChar(0));
		input.append(QChar(0));
		input.append(QChar(0));
		input.append(QChar(0));

		qint64 seconds = QDateTime::currentMSecsSinceEpoch() / 1000;
		input += d->phoneNumber.toLatin1() + d->challengeData + QString::number(seconds).toLatin1();

		d->challengeData.clear();
		ret = d->outputKey->encodeMessage(input, 0, input.length(), false);
	}
	return ret;
}

ProtocolNode* WhatsAppController::createAuthResponseNode()
{
	return new ProtocolNode("response", QMap<QString,QString>(), QList<ProtocolNode*>(), this->authenticate());
}

ProtocolNode* WhatsAppController::createAuthNode()
{
	QByteArray data = this->createAuthBlob();
	QMap<QString, QString> attributes;
	attributes["mechanism"] = "WAUTH-2";
	attributes["user"] = d->phoneNumber;
	ProtocolNode *node = new ProtocolNode("auth", attributes, QList<ProtocolNode*>(), data);
	return node;
}

void WhatsAppController::onConnected()
{
	LogHelper::log("connected");
	login();
}

void WhatsAppController::onDisconnected()
{

}

void WhatsAppController::onBytesWritten(qint64)
{

}

void WhatsAppController::onReadyRead()
{
	while (pollMessage()) {
	}
}

void WhatsAppController::onError(QAbstractSocket::SocketError error)
{

}
void WhatsAppController::handleGroupV2InfoResponse( ProtocolNode *groupNode, bool fromGetGroups )
{
	QString creator = groupNode->getAttribute("creator");
	QString creation = groupNode->getAttribute("creation");
	QString subject = groupNode->getAttribute("subject");
	QString groupID = groupNode->getAttribute("id");
	QStringList participants;
	QStringList admins;
	if (groupNode->getChild(0) != NULL) {
		foreach( ProtocolNode* child , groupNode->getChildren() ) {
			participants.append( child->getAttribute("jid") );
			if (child->getAttribute("type").compare("admin") == 0)
				admins.append(child->getAttribute("jid"));
		}
	}
	emit onGetGroupV2Info(
		d->phoneNumber,
		groupID,
		creator,
		creation,
		subject,
		participants,
		admins,
		fromGetGroups);
}

void WhatsAppController::getStatus(const QStringList& numbers)
{
    QList<ProtocolNode*> children;

    foreach(QString item, numbers) {
        QMap<QString, QString> attributes;
        attributes["jid"] = this->getJID(item);
        ProtocolNode* node = new ProtocolNode("user", attributes, QList<ProtocolNode*>(), QByteArray());
        children.append(node);
    }

    QMap<QString, QString> parentAttributes;
    parentAttributes["to"] = Constants::WHATSAPP_SERVER;
    parentAttributes["type"] = "get";
    parentAttributes["xmlns"] = "status";
    parentAttributes["id"] = this->createMsgId();

    ProtocolNode *eldestChild = new ProtocolNode("status", QMap<QString, QString>(), children, QByteArray());
    QList<ProtocolNode*> eldestGroup;
    eldestGroup.append(eldestChild);

    ProtocolNode *parentNode = new ProtocolNode("iq", parentAttributes, eldestGroup, QByteArray());
    this->sendNode(parentNode);
}

void WhatsAppController::getLastSeen(const QString& number)
{
    QString msgId = this->createMsgId("lastseen");
    ProtocolNode* queryNode = new ProtocolNode("query", QMap<QString, QString>(), QList<ProtocolNode*>(), QByteArray());
    
    QMap<QString, QString> attributes;
    attributes["to"] = this->getJID(number);
    attributes["type"] = "get";
    attributes["id"] = msgId;
    attributes["xmlns"] = "jabber:iq:last";

    QList<ProtocolNode*> children;
    children.append(queryNode);

    ProtocolNode* messageNode = new ProtocolNode("iq", attributes, children, QByteArray());
    this->sendNode(messageNode);
}

void WhatsAppController::getProfilePicture( const QString& number, bool large)
{
    QString msgId = this->createMsgId("getpicture");
    QMap<QString, QString> attributes;
    attributes["type"] = "image";
    if (!large) {
        attributes["type"] = "preview";
    }
    
    ProtocolNode* picture = new ProtocolNode("picture", attributes, QList<ProtocolNode*>(), QByteArray());
    
    QMap<QString, QString> nodeAttributes;
    nodeAttributes["id"] = msgId;
    nodeAttributes["type"] = "get";
    nodeAttributes["xmlns"] = "w:profile:picture";
    nodeAttributes["to"] = this->getJID(number); // get jId;
    
    QList<ProtocolNode*> children;
    children.append( picture );

    ProtocolNode* node = new ProtocolNode("iq", nodeAttributes, children, QByteArray());
    this->sendNode(node);

}

/**
* Process number/jid and turn it into a JID if necessary
*
* @param string $number
*  Number to process
* @return string
*/
QString WhatsAppController::getJID( const QString& number )
{
    QString ret( number.data(), number.length() );
    if ( ret.indexOf('@') < 0 ) {
        //check if group message
        if (ret.indexOf('-') >= 0) {
            //to group
            ret += "@" + Constants::WHATSAPP_GROUP_SERVER;
        }
        else {
            //to normal user
            ret += "@" + Constants::WHATSAPP_SERVER;
        }
    }

    return ret;
}

}//namespace whatsapptracker


