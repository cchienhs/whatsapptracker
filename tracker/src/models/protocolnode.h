#ifndef WHATSAPPTRACKER_PROTOCOLNODE_H
#define WHATSAPPTRACKER_PROTOCOLNODE_H

namespace whatsapptracker {

struct ProtocolNodePrivate;
class ProtocolNode
{
public:
    ProtocolNode( const QString& tag, const QMap<QString, QString>& attributeHash, const QList<ProtocolNode*>& children, const QByteArray& data );
	~ProtocolNode();

    QString nodeString( QString indent="", bool isChild=false );
    bool nodeIdContains( const QString& needle );
    void refreshTimes( int offset=0 );
    QString toString() const;
    bool hasChild( const QString& tag );

    //getters
    QByteArray getData() const;
    QString getTag() const;
    QString getAttribute( const QString& attribute ) const;
    QMap<QString,QString> getAttributes() const;
    QList<ProtocolNode*> getChildren() const;
    ProtocolNode* getChild( const QString& tag );

private:
    ProtocolNodePrivate *d;
};
}

#endif
