#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "calltaghandler.h"

namespace whatsapptracker {

void CallTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (node->getChild(0)->getTag().compare("offer") == 0 ) {
		QString callId = node->getChild(0)->getAttribute("call-id");
		controller->sendReceipt(node, QString(), QString(), callId);

		controller->onCallReceived(
			controller->phoneNumber(),
			node->getAttribute("from"),
			node->getAttribute("id"),
			node->getAttribute("notify"),
			node->getAttribute("t"),
			node->getChild(0)->getAttribute("call-id"));
	}
	else {
		controller->sendAck(node, "call");
	}
}
}