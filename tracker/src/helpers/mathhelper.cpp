#include <QtGlobal>
#include "mathhelper.h"

namespace whatsapptracker {

int MathHelper::randInt(int low, int high)
{
	// Random number between low and high
	return qrand() % ((high + 1) - low) + low;
}

QByteArray MathHelper::uLongToQByteArray(unsigned long input)
{
	unsigned int size = sizeof(unsigned long);
	QByteArray ret(size, 0);
	for (unsigned int i = 0; i < size; ++i) {
		ret[i] = input >> ( (size-i-1)*8);
	}
	return ret;
}
}