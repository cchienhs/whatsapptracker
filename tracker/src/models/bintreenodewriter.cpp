#include "bintreenodewriter.h"
#include "protocolnode.h"
#include "tokenmap.h"
#include "keystream.h"

namespace whatsapptracker{

struct BinTreeNodeWriterPrivate
{
	QString output;
	KeyStream *key = NULL;
};

BinTreeNodeWriter::BinTreeNodeWriter() : 
	d(new BinTreeNodeWriterPrivate)
{
}

BinTreeNodeWriter::~BinTreeNodeWriter()
{
	delete d;
}

void BinTreeNodeWriter::resetKey()
{
	d->key = NULL;
}

void BinTreeNodeWriter::setKey(KeyStream *key)
{
	d->key = key;
}

QString BinTreeNodeWriter::startStream(const QString& domain, const QString& resource)
{
	QMap< QString, QString > attributes;
	attributes["to"] = domain;
	attributes["resource"] = resource;
	this->writeListStart( attributes.size() * 2 + 1);

	d->output += QChar(0x01);
	this->writeAttributes( attributes );

	QString ret = "WA" + this->writeInt8(1) + this->writeInt8(5) + this->flushBuffer();
	return ret;
}

QString BinTreeNodeWriter::writeInt8( int v )
{
	QString ret = QChar( v & 0xff ) ;
	return ret;
}

QString BinTreeNodeWriter::writeInt16( int v )
{
	QString ret = QChar( (v & 0xff00) >> 8 );
	ret += QChar( (v & 0x00ff) >> 0 );
	return ret;
}

QString BinTreeNodeWriter::writeInt24( int v )
{
	QString ret = QChar((v & 0xff0000) >> 16);
	ret += QChar((v & 0x00ff00) >> 8);
	ret += QChar((v & 0x0000ff) >> 0);
	return ret;
}

QString BinTreeNodeWriter::getInt24( int length )
{
	QString ret;
	ret += QChar( (length & 0xf0000) >> 16 );
	ret += QChar( (length & 0xff00) >> 8 );
	ret += QChar( (length & 0xff) );
	return ret;
}

QString BinTreeNodeWriter::write( const ProtocolNode *node, bool encrypt )
{
	if (node == NULL) {
		d->output += QChar(0x00);
	}
	else {
		this->writeInternal(node);
	}

	return this->flushBuffer(encrypt);
}

void BinTreeNodeWriter::writeInternal( const ProtocolNode *node )
{
	unsigned int len = 1;
	if (!node->getAttributes().isEmpty()) {
		len += node->getAttributes().size() * 2;
	}
	if (node->getChildren().length() > 0) {
		len += 1;
	}
	if ( node->getData().length() > 0) {
		len += 1;
	}
	this->writeListStart(len);
	this->writeString(node->getTag());
	this->writeAttributes(node->getAttributes());
	if (node->getData().length() > 0) {
		this->writeBytes(node->getData());
	}
	if (node->getChildren().length()) {
		this->writeListStart(node->getChildren().length());
		ProtocolNode *child;
		foreach(child, node->getChildren()) {
			this->writeInternal(child);
		}
	}
}

void BinTreeNodeWriter::writeAttributes( QMap<QString,QString> attributes) 
{
	QMapIterator<QString, QString> iter(attributes);
	while (iter.hasNext()) {
		iter.next();
		this->writeString(iter.key());
		this->writeString(iter.value());
	}
}

void BinTreeNodeWriter::writeToken( int token )
{
	if (token < 0xf5) {
		d->output += QChar(token);
	} 
	else if( token <= 0x1f4 ) {
		d->output += "\xfe" + QString( token - 0xf5 );
	}
}

void BinTreeNodeWriter::writeJid( const QString& user, const QString& server )
{
	d->output += QChar(0xfa);
	if (user.length() > 0) {
		this->writeString(user);
	}
	else {
		this->writeToken(0);
	}
	this->writeString(server);
}

void BinTreeNodeWriter::writeBytes( const QByteArray& bytes )
{
	int len = bytes.length();
	if (len >= 0x100) {
		d->output += QChar(0xfd);
		d->output += this->writeInt24(len);
	}
	else {
		d->output += QChar(0xfc);
		d->output += this->writeInt8(len);
	}

	for (unsigned int i = 0; i < bytes.length(); ++i)
		d->output += QChar(bytes[i]);
}

void BinTreeNodeWriter::writeString( const QString& tag )
{
	int intVal = -1;
	bool subdict = false;
	if (TokenMap::tryGetToken( tag, subdict, intVal)) {
		if (subdict) {
			this->writeToken(236);
		}
		this->writeToken(intVal);
		return;
	}
	int index = tag.indexOf('@');
	if ( index >= 0 ) {
		QString server = tag.right( tag.length() - (index + 1) );
		QString user = tag.left( index );
		this->writeJid( user, server );
	}
	else {
		this->writeBytes( tag.toLatin1() );
	}
}

QString BinTreeNodeWriter::flushBuffer( bool encrypt )
{
	int size = d->output.length();
	QByteArray data = d->output.toLatin1();
	QString bsize;

	if ( d->key!= NULL && encrypt) {
		QString bsize = this->getInt24(size);
		//encrypt
		data = d->key->encodeMessage( data, size, 0, size);
		unsigned int len = data.length();
		bsize[0] = QChar((8 << 4) | ((len & 16711680) >> 16));
		bsize[1] = QChar((len & 65280) >> 8);
		bsize[2] = QChar(len & 255);
		size = this->parseInt24(bsize);
	}
	QString ret = this->writeInt24(size) + QString::fromLatin1( data.constData(), data.length());
	d->output = "";
	return ret;
}
	
unsigned int BinTreeNodeWriter::parseInt24( const QString& data )
{
	unsigned int ret = 0;
	QByteArray localData = data.toLatin1();
	ret = localData[0] << 16;
	ret |= localData[1] << 8;
	ret |= localData[2] << 0;
	return ret;
}

void BinTreeNodeWriter::writeListStart( int len )
{
	if (len == 0) {
		d->output += QChar(0x00);
	} 
	else if(len < 256) {
		d->output += QChar(0xf8);
		d->output += QChar::fromLatin1(len);
	}
	else {
		d->output += QChar(0xf9) + this->writeInt16(len);
	}
}
}//namespace