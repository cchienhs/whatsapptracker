#ifndef WHATSAPPTRACKER_ACKTAGHANDLER_H
#define WHATSAPPTRACKER_ACKTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class AckTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif