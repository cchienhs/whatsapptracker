#include <QMap>
#include <QString>

#if defined(_WIN32)
#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib")
#endif

#include <mongo/client/dbclient.h>
#include "mongoworker.h"

namespace whatsapptracker {

static bool mongoInitialized = false;

struct MongoWorkerPrivate
{
    mongo::DBClientConnection connection;
    QString dbName;
};

MongoWorker::MongoWorker(QObject* parent)
    :DbWorkerInterface(parent),
    d(new MongoWorkerPrivate)
{
    if (!mongoInitialized) {
        mongoInitialized = true;
        mongo::Status status = mongo::client::initialize();
    }
}

MongoWorker::~MongoWorker()
{
    delete d;
}


void MongoWorker::insertTrackedNumber(const TrackedNumber& TrackedNumber)
{

}

void MongoWorker::updateTrackedNumber(const TrackedNumber& TrackedNumber)
{

}

void MongoWorker::deleteTrackedNumber(const QString& phoneNumber)
{

}

void MongoWorker::getTrackedNumber(const QString& phoneNumber)
{

}

void MongoWorker::getUsers()
{
    QString collectionName = "user";
    QString fullName = d->dbName + "." + collectionName;
    std::auto_ptr<mongo::DBClientCursor> cursor = d->connection.query(fullName.toStdString(), mongo::BSONObj());
    QList<User> list;

    while (cursor->more()) {
        mongo::BSONObj p = cursor->next();
        QString email = QString::fromLatin1( p.getStringField("email") );
        QString phoneNumber = QString::fromLatin1(p.getStringField("phoneNumber"));
        
        mongo::BSONElement bsonEle;
        p.getObjectID(bsonEle);        
        QString id = QString::fromStdString(bsonEle.OID().toString());
        
        bool paidMember = p.getBoolField("paidMemeber");
        QMap<QString,QPair<QString,bool>> phoneNumbersToTrack;

        std::vector<mongo::BSONElement> elements = p["phoneNumbersToTrack"].Array();
        for (std::vector<mongo::BSONElement>::iterator it = elements.begin(); it != elements.end(); ++it) {
            if (it->isABSONObj()) {
                mongo::BSONObj temp = it->embeddedObject();
                QString phoneNumber = QString::fromLatin1( temp.getStringField("phoneNumber") );
                QPair<QString, bool> item;
                item.first = temp.getStringField("nickName");
                item.second = temp.getBoolField("track");
                phoneNumbersToTrack[phoneNumber] = item;
            }
        }

        User user(email, phoneNumber, phoneNumbersToTrack, paidMember, id);
        list.append(user);
    }

    emit onGetUsers(list);
}

void MongoWorker::updateUser(const User& user)
{
    if (userExists(user)){
        QString collectionName = "user";
        QString fullName = d->dbName + "." + collectionName;

        mongo::Query query = MONGO_QUERY("email" << user.email().toStdString());

        mongo::BSONObjBuilder builder;
        builder.append("email", user.email().toStdString());
        builder.append("phoneNumber", user.phoneNumber().toStdString());
        builder.append("paidMember", user.paidMember());
        std::vector<mongo::BSONElement> elements;

        mongo::BSONArrayBuilder arrayBuilder;
        QMap<QString,QPair<QString, bool>> numbersToTrack = user.phoneNumbersToTrack();
        Q_FOREACH(QString key, numbersToTrack.keys()) {
            QPair<QString, bool> item = numbersToTrack.value(key);            
            arrayBuilder.append(BSON("phoneNumber" << key.toStdString() << "nickName" << item.first.toStdString() << "track" << item.second));
        }

        builder.append("phoneNumbersToTrack", arrayBuilder.arr());
        d->connection.update(fullName.toStdString(), query, builder.obj());
        emit onUpdateUser(true);
    }
    else {
        emit onUpdateUser(false);
    }
}

bool MongoWorker::userExists(const User& user)
{
    QString collectionName = "user";
    QString fullName = d->dbName + "." + collectionName;

    mongo::Query query = MONGO_QUERY("email" << user.email().toStdString());
    std::auto_ptr<mongo::DBClientCursor> cursor = d->connection.query(fullName.toStdString(), query);
    if (cursor->more())
        return true;
    else
        return false;
}

void MongoWorker::getConfig()
{
    QString collectionName = "config";
    QString fullName = d->dbName + "." + collectionName;
    std::auto_ptr<mongo::DBClientCursor> cursor = d->connection.query(fullName.toStdString(), mongo::BSONObj());
    whatsapptracker::Config config;

    //there is suppose to be only one document in the config collection. In any case, even if there are multiples, only the first one is retrieved.
    if (cursor->more()) {
        mongo::BSONObj p = cursor->next();
        QString phoneNumber = QString::fromLatin1(p.getStringField("phoneNumber"));
        QString secret = QString::fromLatin1(p.getStringField("secret"));
        QString nickName = QString::fromLatin1(p.getStringField("nickName"));
        unsigned int databaseLookupRefreshRate = p.getIntField("databaseLookupRefreshRate");
        
        config = Config(phoneNumber, secret, nickName, databaseLookupRefreshRate);
    }
    emit onGetConfig(config);
}

void MongoWorker::updateConfig(const Config& config)
{
    QString collectionName = "config";
    QString fullName = d->dbName + "." + collectionName;
    std::auto_ptr<mongo::DBClientCursor> cursor = d->connection.query(fullName.toStdString(), mongo::BSONObj());
    
    //there is suppose to be only one document in the config collection. In any case, even if there are multiples, only the first one is retrieved.
    if (cursor->more()) {
        mongo::BSONObj p = cursor->next();
        mongo::BSONElement element;
        p.getObjectID(element);
        
        mongo::Query query = MONGO_QUERY("_id" << element);
        mongo::BSONObjBuilder builder;
        builder.append("phoneNumber", config.phoneNumber().toStdString());
        builder.append("secret", config.secret().toStdString());
        builder.append("nickName", config.nickName().toStdString());
        builder.append("databaseLookupRefreshRate", config.databaseLookupRefreshRate());
        d->connection.update(fullName.toStdString(), query, builder.obj());
        emit onUpdateConfig(true);
    }
    else {
        emit onUpdateConfig(false);
    }
}

void MongoWorker::connect(const QString& url, const QString& dbName, const QString& email, const QString& password)
{
    d->dbName = dbName;
    std::string error;

    d->connection.connect(url.toStdString(), error);
    d->connection.auth(dbName.toStdString(), email.toStdString(), password.toStdString(), error);
}
}