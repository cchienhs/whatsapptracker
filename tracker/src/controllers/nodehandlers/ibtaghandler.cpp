#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "../../helpers/loghelper.h"
#include "ibtaghandler.h"

namespace whatsapptracker {

void IbTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	foreach( ProtocolNode* child, node->getChildren() ) {
		QString tag = child->getTag();
		
		if (tag.compare("dirty") == 0) {
			QStringList attributes;
			attributes << child->getAttribute("type");
			controller->sendClearDirty(attributes);
			break;
		}
		else if (tag.compare("account") == 0) {
			controller->onPaymentReceived(
				controller->phoneNumber(),
				child->getAttribute("kind"),
				child->getAttribute("status"),
				child->getAttribute("creation"),
				child->getAttribute("expiration"));
		}
		else if (tag.compare("offline") == 0) {
			//do nothing for the moment
		}
		else {
			LogHelper::log("ib handler for " + tag + " not implemented");
		}
	}
}
}