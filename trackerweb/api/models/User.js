/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  identity: 'User',
  attributes: {
      email: {
          type: 'email'
      },
      phoneNumber: {
          type: 'string',
          unique: false
      },
      paidMember: {
          type: 'boolean'
      },
      phoneNumbersToTrack: {
          type: 'array'
          //phoneNumber: string
          //nickName: string
          //track: boolean
      },

      deepCopy: function( rhs )
      {
          this.email = rhs.email;
          this.phoneNumber = rhs.phoneNumber;
          this.paidMember = rhs.paidMember;
          /*this.phoneNumbersToTrack.splice(0, this.phoneNumbersToTrack.length);

          for( var i=0; i<rhs.phoneNumbersToTrack.length; ++i ) {
           this.phoneNumbersToTrack.push( rhs.phoneNumbersToTrack[i]);
          }*/
      }
  }

};

