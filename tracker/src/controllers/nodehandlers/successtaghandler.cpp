#include <QTime>

#include "../../helpers/filesystemhelper.h"
#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "../../models/bintreenodewriter.h"
#include "../../constants.h"
#include "successtaghandler.h"

namespace whatsapptracker {

void SuccessTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (node->getAttribute("status").compare("active") == 0) {
		controller->setLoginStatus( Constants::CONNECTED_STATUS );
        controller->setLoginTime(QDateTime::currentDateTime());
		QByteArray challengeData = node->getData();
		FileSystemHelper::writeContent(controller->challengeFilename(), challengeData);
		controller->writer()->setKey(controller->outputKey());
		controller->onLoginSuccess(controller->phoneNumber(), node->getAttribute("kind"), node->getAttribute("status"),
			node->getAttribute("creation"), node->getAttribute("expiration"));
	}
	else if (node->getAttribute("status").compare("expired") == 0) {
		controller->onAccountExpired(controller->phoneNumber(), node->getAttribute("kind"), node->getAttribute("status"),
			node->getAttribute("creation"), node->getAttribute("expiration"));
	}
}
}