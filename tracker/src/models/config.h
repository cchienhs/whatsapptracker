#ifndef WHATSAPPTRACKER_CONFIG_H
#define WHATSAPPTRACKER_CONFIG_H

#include <QObject>
namespace whatsapptracker {

struct ConfigPrivate;
class Config : public QObject
{
public:
    Config(QObject* parent = NULL);
    Config(const QString& phoneNumber, const QString& secret, const QString& nickName, const unsigned int databaseLookupRefreshRate, QObject* parent = NULL);
    Config(const Config& rhs);
    ~Config();

    Config& operator=(const Config& rhs);
    
    void setPhoneNumber(const QString& value);
    QString phoneNumber() const;
    void setSecret(const QString& value);
    QString secret() const;
    void setNickName(const QString& value);
    QString nickName() const;
    void setDatabaseLookupRefreshRate(const unsigned int value);
    unsigned int databaseLookupRefreshRate() const;


private:
    ConfigPrivate* d;
};
}
Q_DECLARE_METATYPE(whatsapptracker::Config)

#endif