#include <QString>

namespace whatsapptracker {

class Constants
{
public:
    static const QString CONNECTED_STATUS; // Describes the connection status with the WhatsApp server.
    static const QString DISCONNECTED_STATUS; // Describes the connection status with the WhatsApp server.
    static const QString MEDIA_FOLDER; // The relative folder to store received media files
    static const QString PICTURES_FOLDER; // The relative folder to store picture files
    static const QString DATA_FOLDER; // The relative folder to store cache files.
    static const int PORT = 443; // The port of the WhatsApp server.
    static const int TIMEOUT_SEC = 1; // The timeout for the connection with the WhatsApp servers.
    static const int TIMEOUT_USEC = 0;
    static const QString WHATSAPP_CHECK_HOST; // The check credentials host.
    static const QString WHATSAPP_GROUP_SERVER; // The Group server hostname
    static const QString WHATSAPP_REGISTER_HOST; // The register code host.
    static const QString WHATSAPP_REQUEST_HOST; // The request code host.
    static const QString WHATSAPP_SERVER; // The hostname used to login/send messages.
    static const QString WHATSAPP_DEVICE; // The device name.
    static const QString WHATSAPP_VER; // The WhatsApp version.
    static const QString WHATSAPP_USER_AGENT; // User agent used in request/registration code.
    static const QString WHATSAPP_VER_CHECKER;
};
}
