#include <QMessageAuthenticationCode>
#include "../helpers/loghelper.h"
#include "../helpers/cryptohelper.h"
#include "keystream.h"
#include "rc4.h"

namespace whatsapptracker {

static const unsigned int DROP = 768;
static const QString AuthMethod = "WAUTH-2";

struct KeyStreamPrivate
{	
	RC4 *rc4;
	unsigned int seq = 0;
	QByteArray macKey;
};

KeyStream::KeyStream( const QByteArray& key, const QByteArray& macKey )
	: d( new KeyStreamPrivate )
{
	d->rc4 = new RC4(key, DROP);
	d->macKey = QByteArray(macKey.constData(), macKey.length());
}

KeyStream::KeyStream(const KeyStream &obj)
	:d( new KeyStreamPrivate )
{

}

KeyStream::~KeyStream()
{
	delete d;
}


QList<QByteArray> KeyStream::generateKeys( const QString& password, const QByteArray& nonce )
{
	QList<QByteArray> array1;
	array1 << QByteArray() << QByteArray() << QByteArray() << QByteArray();
	QList<char> array2;
	array2 << 1 << 2 << 3 << 4;

	QByteArray localNonce = QByteArray(nonce.constData(), nonce.length());
	localNonce.append( QChar(0) );

	for ( unsigned int j = 0; j < array1.length(); j++ ) {
		localNonce[localNonce.length() - 1] = array2[j];
		QByteArray foo = CryptoHelper::pbkdf2( QCryptographicHash::Sha1, password, QString::fromLatin1(localNonce), 2, 20, true);
		array1[j] = foo;
	}
	return array1;
}

QByteArray KeyStream::computeMac( const QByteArray& buffer, unsigned int offset, unsigned int length )
{
	QByteArray localBuffer = buffer.mid(offset, length);
	localBuffer += d->seq >> 24;
	localBuffer += d->seq >> 16;
	localBuffer += d->seq >> 8;
	localBuffer += d->seq;
	d->seq++;
	return QMessageAuthenticationCode::hash(localBuffer, d->macKey, QCryptographicHash::Sha1);
}

QByteArray KeyStream::decodeMessage( const QByteArray& buffer, unsigned int macOffset, unsigned int offset, unsigned int length )
{
	QByteArray mac = this->computeMac( buffer, offset, length);
	
	//validate mac
	for (unsigned char i = 0; i<4; i++) {
		unsigned char foo = buffer[macOffset + i];
		unsigned char bar = mac[i];
		if (foo != bar) {
			LogHelper::log("MAC mismatch: $foo != $bar");
		}
	}
	return d->rc4->cipher(buffer, offset, length);
}

QByteArray KeyStream::encodeMessage( const QByteArray& buffer, unsigned int macOffset, unsigned int offset, unsigned int length )
{
	QByteArray data =  d->rc4->cipher(buffer, offset, length);
	QByteArray mac = this->computeMac(data, offset, length);
	QByteArray ret = data.left(macOffset) + mac.left(4) + data.right(data.length() - (macOffset + 4));
	return ret;
}
}//namespace
