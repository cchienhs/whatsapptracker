#include <QStringList>
#include <QTime>
#include <QList>
#include <QMap>

#include "protocolnode.h"

namespace whatsapptracker {

struct ProtocolNodePrivate
{
public:
    QString tag;
    QMap<QString,QString> attributeHash;
    QList<ProtocolNode*> children;
    QByteArray data;
};

ProtocolNode::ProtocolNode( const QString& tag, const QMap<QString,QString>& attributeHash, const QList<ProtocolNode*>& children, const QByteArray& data )
    : d( new ProtocolNodePrivate )
{
    d->tag = tag;
    d->attributeHash = attributeHash;
    d->children = children;
    d->data = data;
}

ProtocolNode::~ProtocolNode()
{
	delete d;
}

QByteArray ProtocolNode::getData() const
{
    return d->data;
}

QString ProtocolNode::getTag() const
{
    return d->tag;
}

QMap<QString,QString> ProtocolNode::getAttributes() const
{
    return d->attributeHash;
}

QList<ProtocolNode*> ProtocolNode::getChildren() const
{
    return d->children;
}

/**
 * @param string $needle
 * @return boolean
 */
bool ProtocolNode::nodeIdContains( const QString& needle )
{
    return ( this->getAttribute( "id" ).indexOf( needle ) > (-1) );
}

/**
 * @function void ProtocolNode::refreshTimes( int offset=0 )
 * @param int $offset
 */
void ProtocolNode::refreshTimes( int offset )
{
    if( d->attributeHash.contains( "id" ) ) {
        QString id = d->attributeHash[ "id" ];
        QStringList parts = id.split( '-' );
        parts[0] = QTime::currentTime().toString() + QString::number( offset );
        d->attributeHash["id"] = parts.join( '-' );
    }

    if( d->attributeHash.contains( "t" ) ) {
        d->attributeHash["t"] = QTime::currentTime().toString();
    }
}

/**
 * @param $attribute
 * @return string
 */
QString ProtocolNode::getAttribute( const QString& attribute ) const
{
    QString ret;
    if( d->attributeHash.contains( attribute ) ) {
        ret = d->attributeHash[ attribute ];
    }
    return ret;
}

/**
 * Print human readable ProtocolNode object
 *
 * @return string
 */
QString ProtocolNode::toString() const
{
   
    /*
    $readableNode = array(
        'tag'           => $this->tag,
        'attributeHash' => $this->attributeHash,
        'children'      => $this->children,
        'data'          => $this->data
    );
    */
    //return print_r($readableNode, true);
    return "";
}

//get children supports string tag or int index
/**
 * @param $tag
 * @return ProtocolNode
 */
ProtocolNode* ProtocolNode::getChild( const QString& tag )
{
    ProtocolNode *ret = NULL;
    if( d->children.length() ) {
        bool ok;
        int value = tag.toInt(&ok);

        if( ok ) {
            if( d->children.length() > value ) {
                return d->children[value];
            }
            else {
                return NULL;
            }
        }

        ProtocolNode *child;
        foreach( child, d->children ) {
            if( child->getTag().compare( tag ) == 0 ) {
                return child;
            }
            ret = child->getChild( tag );
            if( ret ) {
                return ret;
            }
        }
    }
    return NULL;
}

/**
 * @param tag
 * @return bool
 */
bool ProtocolNode::hasChild( const QString& tag )
{
    return this->getChild(tag) == NULL ? false : true;
}


/**
 * @function QString ProtocolNode::nodeString( QString indent="", bool isChild=false )
 * @param string indent
 * @param bool   isChild
 * @return string
 */
QString ProtocolNode::nodeString( QString indent, bool isChild )
{
    //formatters
    QString lt = "&lt;";
    QString gt = "&gt;";
    QString nl = "<br />";

    indent = indent.replace( " ", "&nbsp;" );

    QString ret = indent + lt + d->tag;
    QMap<QString,QString>::const_iterator i = d->attributeHash.constBegin();

    while (i != d->attributeHash.constEnd()) {
        ret += " " + i.key() + "=\"" + i.value() + "\"";
        ++i;
    }

    ret += gt;
    if( d->data.length() > 0 ) {
        if( d->data.length() <= 1024 ) {
           ret += d->data;
        }
        else {
            ret += " " + QString::number(d->data.length()) + " byte data";
        }
    }
    if( d->children.length() ) {
        ret += nl;
        QStringList foo;
        ProtocolNode *child;
        foreach( child, d->children ) {
            foo << child->nodeString( indent + " ", true );
        }
        ret += foo.join( nl );
        ret += nl + indent;
    }
    ret += lt + "/" + d->tag + gt;

    if( !isChild ) {
        ret += nl;
        ret += nl;
    }

    return ret;
}

}//namespace whatsapptracker

