#include "whatsapphelper.h"

namespace whatsapptracker {

QString WhatsAppHelper::convertJidToPhoneNumber(const QString& input)
{
    QString ret;
    int index = input.indexOf('@');
    if (index >= 0) {
        ret = input.left(index);
        QByteArray data = ret.toLatin1();
        for (unsigned int i = 0; i < data.length(); ++i)
            data[i] = data[i] + 48;
        ret = QString::fromLatin1(data);
    }
    return ret;
}
}