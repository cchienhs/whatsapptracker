#ifndef WHATSAPPTRACKER_PRESENCETAGHANDLER_H
#define WHATSAPPTRACKER_PRESENCETAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class PresenceTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif