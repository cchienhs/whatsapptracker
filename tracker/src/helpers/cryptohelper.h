#ifndef WHATSAPPTRACKER_CRYPTOHELPER_H
#define WHATSAPPTRACKER_CRYPTOHELPER_H

#include <QByteArray>
#include <QCryptographicHash>
#include <QString>

namespace whatsapptracker {

class CryptoHelper
{
public:
	static QByteArray CryptoHelper::pbkdf2(const QCryptographicHash::Algorithm& algorithm, const QString& password, const QString& salt, unsigned int count, unsigned int keyLength, bool rawOutput = false);
};

}//namespace
#endif