#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "receipttaghandler.h"

namespace whatsapptracker {

void ReceiptTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (node->hasChild("list")) {
		foreach(ProtocolNode *child, node->getChild("list")->getChildren()) {
			controller->onMessageReceivedClient(controller->phoneNumber(),
				node->getAttribute("from"),
				child->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("participant"));
		}
	}

	controller->onMessageReceivedClient(controller->phoneNumber(),
		node->getAttribute("from"),
		node->getAttribute("id"),
		node->getAttribute("type"),
		node->getAttribute("t"),
		node->getAttribute("participant"));

	controller->onGetReceipt(node->getAttribute("from"),
		node->getAttribute("id"),
		node->getAttribute("offline"),
		node->getAttribute("retry"));

	controller->sendAck(node, "receipt");
}
}

