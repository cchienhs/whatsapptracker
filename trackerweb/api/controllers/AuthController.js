var passport = require('passport');

module.exports = {

    login: function( req, res ) {
        res.view();
    },

    logout: function (req, res){
        req.session.user = null;
        req.session.flash = 'You have logged out';
        res.redirect('auth/login');
    },

    'loginFacebook': function (req, res, next) {
        passport.authenticate('facebook', { scope: ['email', 'user_about_me']},
            function (err, user) {
                if(err) {
                    req.session.flash = 'There was an error';
                    req.session.authenticated = false;
                    res.redirect('auth/login');
                } else {
                    req.session.user = user;
                    req.session.authenticated = true;
                    res.redirect('/user/dashboard');
                }
            })(req, res, next);
    },

    'loginFacebook/callback': function (req, res, next) {
        passport.authenticate('facebook',
            function (req, res) {
                res.redirect('/user/dashboard');
            })(req, res, next);
    }

};
