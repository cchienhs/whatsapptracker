#include <QTimer>
#include <QPair>

#include "db/dbworkerinterface.h"
#include "db/mongoworker.h"
#include "dbcontroller.h"

namespace whatsapptracker {

struct  DbControllerPrivate
{
    QString url;
    QString username;
    QString password;
    QString dbName;
    DbController::DatabaseType dbType;
    DbWorkerInterface *worker = NULL;
    QList<GetUsersCallback*> getUsersCallers;
    QList< QPair<User, UpdateUserCallback*>> updateUserWaitList; //list of callers whose invokation has yet ot be triggered
    QList< QPair<User, UpdateUserCallback*>> updateUserResponseList; //list of callers whose invokation has been triggered, but not yet completed
    QList<GetConfigCallback*> getConfigCallers;
    QList<QPair<Config, UpdateConfigCallback*>> updateConfigWaitList;
    QList<QPair<Config, UpdateConfigCallback*>> updateConfigResponseList;
};

DbController::DbController( QObject* parent )
	: QThread( parent ),
	  d(new DbControllerPrivate)
{
    qRegisterMetaType<QList<whatsapptracker::User>>("QList<whatsapptracker::User>");
    qRegisterMetaType<QList<whatsapptracker::User>>("QList<User>");
    qRegisterMetaType<whatsapptracker::User>("User");
    qRegisterMetaType<whatsapptracker::Config>("Config");
}

DbController::~DbController()
{
	delete d;
}

void DbController::setup(const QString& url, const QString& dbName, const QString& username, const QString& password, DatabaseType dbType)
{
    d->url = url;
    d->username = username;
    d->password = password;
    d->dbName = dbName;
    d->dbType = dbType;
}

void DbController::getUsers( GetUsersCallback* callback)
{
    d->getUsersCallers.append(callback);
    getUsers();
}

void DbController::getUsers()
{
    if (d->worker == NULL) {
        QTimer::singleShot(10, this, SLOT(getUsers()));
    }
    else {
        emit getUsersSignal();
    }
}

void DbController::updateUser(const User& user, UpdateUserCallback* callback)
{
    d->updateUserWaitList.append(QPair<User, UpdateUserCallback*>(user, callback));
    updateUser();
}

void DbController::updateUser()
{
    if (d->worker == NULL) {
        QTimer::singleShot(10, this, SLOT(updateUser()));
    }
    else {
        QPair<User, UpdateUserCallback*> item = d->updateUserWaitList.takeFirst();
        d->updateUserResponseList.append(item);
        emit updateUserSignal(item.first);
    }
}

void DbController::onGetUsers(const QList<User>& users)
{
    Q_FOREACH(GetUsersCallback* callback, d->getUsersCallers) {
        callback->onGetUsers(users);
        d->getUsersCallers.removeFirst();
    }
}

void DbController::onUpdateUser(const bool status)
{
    QPair<User, UpdateUserCallback*> item = d->updateUserResponseList.takeFirst();
    if (item.second != NULL) {
        UpdateUserCallback* callback = item.second;
        callback->onUpdateUser(status);
    }

    if (d->updateUserWaitList.length() > 0) {
        updateUser();
    }
}

void DbController::getConfig(GetConfigCallback* callback)
{
    d->getConfigCallers.append(callback);
    getConfig();
}

void DbController::getConfig()
{
    if (d->worker == NULL) {
        QTimer::singleShot(10, this, SLOT(getconfig()));
    }
    else {
        emit getConfigSignal();
    }
}

void DbController::onGetConfig(const Config& config)
{
    Q_FOREACH(GetConfigCallback* callback, d->getConfigCallers) {
        callback->onGetConfig(config);
        d->getConfigCallers.removeFirst();
    }
}

void DbController::updateConfig(const Config& config, UpdateConfigCallback* callback)
{
    d->updateConfigWaitList.append(QPair<Config, UpdateConfigCallback*>(config, callback));
    updateConfig();
}

void DbController::updateConfig()
{
    if (d->worker == NULL) {
        QTimer::singleShot(10, this, SLOT(updateConfig()));
    }
    else {
        QPair<Config, UpdateConfigCallback*> item = d->updateConfigWaitList.takeFirst();
        d->updateConfigResponseList.append(item);
        emit updateConfigSignal(item.first);
    }
}

void DbController::onUpdateConfig(const bool status)
{
    QPair<Config, UpdateConfigCallback*> item = d->updateConfigResponseList.takeFirst();
    if (item.second != NULL) {
        UpdateConfigCallback* callback = item.second;
        callback->onUpdateConfig(status);
    }

    if (d->updateConfigWaitList.length() > 0) {
        updateConfig();
    }
}
void DbController::run()
{
    if (d->dbType == DatabaseType::mongo)
        d->worker = new MongoWorker();    

    if (d->worker != NULL) {
    
        //for getting users
        QObject::connect(this, SIGNAL(getUsersSignal()), d->worker, SLOT(getUsers()), Qt::QueuedConnection);
        QObject::connect(d->worker, SIGNAL(onGetUsers(const QList<User>&)), this, SLOT(onGetUsers(const QList<User>&)), Qt::QueuedConnection);

        //for updating users
        QObject::connect(this, SIGNAL(updateUserSignal(const User&)), d->worker, SLOT(updateUser(const User&)), Qt::QueuedConnection);
        QObject::connect(d->worker, SIGNAL(onUpdateUser(const bool)), this, SLOT(onUpdateUser(const bool)), Qt::QueuedConnection);

        //for getting config
        QObject::connect(this, SIGNAL(getConfigSignal()), d->worker, SLOT(getConfig()), Qt::QueuedConnection);
        QObject::connect(d->worker, SIGNAL(onGetConfig(const Config&)), this, SLOT(onGetConfig(const Config&)), Qt::QueuedConnection);

        //for updating config
        QObject::connect(this, SIGNAL(updateConfigSignal(const config&)), d->worker, SLOT(updateConfig(const Config&)), Qt::QueuedConnection);
        QObject::connect(d->worker, SIGNAL(onUpdateConfig(const bool)), this, SLOT(onUpdateConfig(const bool)), Qt::QueuedConnection);
        d->worker->connect(d->url, d->dbName, d->username, d->password);
        exec();
    }
}
}