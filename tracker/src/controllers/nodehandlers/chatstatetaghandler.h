#ifndef WHATSAPPTRACKER_CHATSTATETAGHANDLER_H
#define WHATSAPPTRACKER_CHATSTATETAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class ChatStateTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif