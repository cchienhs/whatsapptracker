#include "rc4.h"

namespace whatsapptracker {

struct RC4Private
{
	unsigned char s[256];
	unsigned char i;
	unsigned char j;
};

RC4::RC4( const QByteArray& key, unsigned int drop )
	:d(new RC4Private)
{
	for (unsigned int i = 0; i < 256; ++i)
		d->s[i] = i;

	for (unsigned int i = 0, j = 0; i < 256; i++) {
		unsigned char k = key[i%key.length()];
		j = (j + k + d->s[i]) & 255;
		this->swap(i, j);
	}
	
	d->i = 0;
	d->j = 0;

	QByteArray range;
	for (unsigned int i = 0; i <= drop; ++i)
		range.append(i);

	this->cipher(range, 0, drop);
}

void RC4::swap( unsigned char i, unsigned char j )
{
	unsigned char c = d->s[i];
	d->s[i] = d->s[j];
	d->s[j] = c;
}

QByteArray RC4::cipher( const QByteArray& data, unsigned int offset, unsigned int length )
{
	QByteArray out(data.constData(), data.length());
	for ( unsigned int n = length; n > 0; n-- ) {
		d->i = (d->i + 1) & 0xff;
		d->j = (d->j + d->s[d->i]) & 0xff;
		this->swap(d->i, d->j);
		unsigned char tempd = data[offset] ;
		out[offset] = (tempd ^ d->s[(d->s[d->i] + d->s[d->j]) & 0xff]);
		offset++;
	}
	return out;
}
}//namespace