#ifndef WHATSAPPTRACKER_IQTAGHANDLER_H
#define WHATSAPPTRACKERI_QTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class IqTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif