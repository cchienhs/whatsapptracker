#include <QMap>
#include "../whatsappcontroller.h"
#include "../../constants.h"
#include "../../models/protocolnode.h"
#include "../../models/messagestoreinterface.h"
#include "messagetaghandler.h"

namespace whatsapptracker {

void MessageTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	controller->messageQueue().append(node);

	if (node->hasChild("x") && controller->lastId().compare(node->getAttribute("id")) == 0) {
		controller->sendNextMessage();
	}
	//outstanding
	/*if ($this->newMsgBind && ($node->getChild('body') || $node->getChild('media'))) {
	$this->newMsgBind->process($node);
	}*/
	if ((node->getAttribute("type").compare("text") == 0) && (node->getChild("body") != NULL)) {
		QString author = node->getAttribute("participant");
		if (author.isEmpty()) {
			controller->onGetMessage(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("notify"),
				node->getChild("body")->getData());

			if (controller->messageStore() != NULL)
				controller->messageStore()->saveMessage(node->getAttribute("from"), controller->phoneNumber(), node->getChild("body")->getData(), node->getAttribute("id"), node->getAttribute("t"));
		}
		else {//group chat message
			controller->onGetGroupMessage(
				controller->phoneNumber(),
				node->getAttribute("from"),
				author,
				node->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("notify"),
				node->getChild("body")->getData());
		}
		if (autoReceipt)
			controller->sendReceipt(node, type, author);
	}
	if ((node->getAttribute("type").compare("text") == 0) && (node->getChild(0)->getTag().compare("en") == 0)) {
		// TODO
		if (autoReceipt) {
			controller->sendReceipt(node, type);
		}
	}
	if ((node->getAttribute("type").compare("media") == 0) && (node->getChild("media") != NULL)) {
		if (node->getChild("media")->getAttribute("type").compare("image") == 0) {
			if (node->getAttribute("participant").isEmpty()) {
				controller->onGetImage(
					controller->phoneNumber(),
					node->getAttribute("from"),
					node->getAttribute("id"),
					node->getAttribute("type"),
					node->getAttribute("t"),
					node->getAttribute("notify"),
					node->getChild("media")->getAttribute("size"),
					node->getChild("media")->getAttribute("url"),
					node->getChild("media")->getAttribute("file"),
					node->getChild("media")->getAttribute("mimetype"),
					node->getChild("media")->getAttribute("filehash"),
					node->getChild("media")->getAttribute("width"),
					node->getChild("media")->getAttribute("height"),
					node->getChild("media")->getData(),
					node->getChild("media")->getAttribute("caption"));
			}
			else {
				controller->onGetGroupImage(
					controller->phoneNumber(),
					node->getAttribute("from"),
					node->getAttribute("participant"),
					node->getAttribute("id"),
					node->getAttribute("type"),
					node->getAttribute("t"),
					node->getAttribute("notify"),
					node->getChild("media")->getAttribute("size"),
					node->getChild("media")->getAttribute("url"),
					node->getChild("media")->getAttribute("file"),
					node->getChild("media")->getAttribute("mimetype"),
					node->getChild("media")->getAttribute("filehash"),
					node->getChild("media")->getAttribute("width"),
					node->getChild("media")->getAttribute("height"),
					node->getChild("media")->getData(),
					node->getChild("media")->getAttribute("caption"));
			}
		}
		else if (node->getChild("media")->getAttribute("type").compare("video") == 0) {
			if (node->getAttribute("participant").isEmpty()) {
				controller->onGetVideo(
					controller->phoneNumber(),
					node->getAttribute("from"),
					node->getAttribute("id"),
					node->getAttribute("type"),
					node->getAttribute("t"),
					node->getAttribute("notify"),
					node->getChild("media")->getAttribute("url"),
					node->getChild("media")->getAttribute("file"),
					node->getChild("media")->getAttribute("size"),
					node->getChild("media")->getAttribute("mimetype"),
					node->getChild("media")->getAttribute("filehash"),
					node->getChild("media")->getAttribute("duration"),
					node->getChild("media")->getAttribute("vcodec"),
					node->getChild("media")->getAttribute("acodec"),
					node->getChild("media")->getData(),
					node->getChild("media")->getAttribute("caption"));
			}
			else {
				controller->onGetGroupVideo(
					controller->phoneNumber(),
					node->getAttribute("from"),
					node->getAttribute("participant"),
					node->getAttribute("id"),
					node->getAttribute("type"),
					node->getAttribute("t"),
					node->getAttribute("notify"),
					node->getChild("media")->getAttribute("url"),
					node->getChild("media")->getAttribute("file"),
					node->getChild("media")->getAttribute("size"),
					node->getChild("media")->getAttribute("mimetype"),
					node->getChild("media")->getAttribute("filehash"),
					node->getChild("media")->getAttribute("duration"),
					node->getChild("media")->getAttribute("vcodec"),
					node->getChild("media")->getAttribute("acodec"),
					node->getChild("media")->getData(),
					node->getChild("media")->getAttribute("caption"));
			}
		}
		else if (node->getChild("media")->getAttribute("type").compare("audio") == 0) {
			QString author = node->getAttribute("participant");
			controller->onGetAudio(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("notify"),
				node->getChild("media")->getAttribute("size"),
				node->getChild("media")->getAttribute("url"),
				node->getChild("media")->getAttribute("file"),
				node->getChild("media")->getAttribute("mimetype"),
				node->getChild("media")->getAttribute("filehash"),
				node->getChild("media")->getAttribute("seconds"),
				node->getChild("media")->getAttribute("acodec"),
				author);

		}
		else if (node->getChild("media")->getAttribute("type").compare("vcard") == 0) {
			QString name, data, author;

			if (node->getChild("media")->hasChild("vcard")) {
				name = node->getChild("media")->getChild("vcard")->getAttribute("name");
				data = node->getChild("media")->getChild("vcard")->getData();
			}
			else {
				name = "NO_NAME";
				data = node->getChild("media")->getData();
			}

			author = node->getAttribute("participant");
			controller->onGetvCard(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("notify"),
				name,
				data,
				author);
		}
		else if (node->getChild("media")->getAttribute("type").compare("location") == 0) {
			QString url = node->getChild("media")->getAttribute("url");
			QString name = node->getChild("media")->getAttribute("name");
			QString author = node->getAttribute("participant");

			controller->onGetLocation(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("type"),
				node->getAttribute("t"),
				node->getAttribute("notify"),
				name,
				node->getChild("media")->getAttribute("longitude"),
				node->getChild("media")->getAttribute("latitude"),
				url,
				node->getChild("media")->getData(),
				author);
		}

		if (autoReceipt) {
			controller->sendReceipt(node, type);
		}
	}
	if (node->getChild("received") != NULL) {
		controller->onMessageReceivedClient(
			controller->phoneNumber(),
			node->getAttribute("from"),
			node->getAttribute("id"),
			node->getAttribute("type"),
			node->getAttribute("t"),
			node->getAttribute("participant"));
	}
	if (node->getAttribute("type").compare("media") && node->getChild(0)->getAttribute("type").compare("image")==0) {
		QString msgId = controller->createMsgId();

		QMap<QString, QString> attributes1;
		attributes1["url"] = node->getChild(0)->getAttribute("url");
		ProtocolNode* ackNode = new ProtocolNode("ack",attributes1,QList<ProtocolNode*>(),QByteArray());

		QMap<QString, QString> attributes2;
		attributes2["id"] = msgId;
		attributes2["xmlns"] = "w:m";
		attributes2["type"] = "set";
		attributes2["to"] = Constants::WHATSAPP_SERVER;

		QList<ProtocolNode*> list2;
		list2.append(ackNode);
		ProtocolNode* iqNode = new ProtocolNode("iq", attributes2, list2, QByteArray());

		controller->sendNode(iqNode);
		delete iqNode;
	}
}
}