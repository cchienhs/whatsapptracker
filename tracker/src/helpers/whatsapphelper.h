#ifndef WHATSAPPTRACKER_WHATSAPPHELPER_H
#define WHATSAPPTRACKER_WHATSAPPHELPER_H

#include <QString>

namespace whatsapptracker {

class WhatsAppHelper
{
public:
    static QString convertJidToPhoneNumber(const QString& input);
};
}
#endif