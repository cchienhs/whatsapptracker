#ifndef WHATSAPPTRACKER_CALLTAGHANDLER_H
#define WHATSAPPTRACKER_CALLTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class CallTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif