#ifndef WHATSAPPTRACKER_IBTAGHANDLER_H
#define WHATSAPPTRACKER_IBTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class IbTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif