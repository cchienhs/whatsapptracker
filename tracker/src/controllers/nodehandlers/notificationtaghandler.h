#ifndef WHATSAPPTRACKER_NOTIFICATIONTAGHANDLER_H
#define WHATSAPPTRACKER_NOTIFICATIONTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class NotificationTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif