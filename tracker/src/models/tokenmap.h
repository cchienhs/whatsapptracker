#ifndef WHATSAPPTRACKER_TOKENMAP_H
#define WHATSAPPTRACKER_TOKENMAP_H

#include <QStringList>
#include <QString>

namespace whatsapptracker {

class TokenMap
{
public:
	static bool tryGetToken(const QString& string, bool& subdict,int& token);
	static void getToken(const int& token, bool& subdict, QString& string);

};

}//namespace
#endif