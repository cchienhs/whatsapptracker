#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "chatstatetaghandler.h"

namespace whatsapptracker {

void ChatStateTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (controller->phoneNumber().compare(node->getAttribute("from").left(controller->phoneNumber().length())) != 0
		&& node->getAttribute("from").indexOf("-") < 0) {
		if (node->getChild(0)->getTag().compare("composing") == 0) {
			controller->onMessageComposing(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				"composing",
				node->getAttribute("t"));
		}
		else {
			controller->onMessagePaused(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				"paused",
				node->getAttribute("t"));
		}
	}
}
}