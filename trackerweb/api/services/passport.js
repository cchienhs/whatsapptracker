var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy;

function findById(id, fn) {
    User.findOneById(id, function (err, user) {
        if (err)
            return fn(null, null);
        else
            return fn(null, user);
        });
}


passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    findById(id, function (err, user) {
        done(err, user);
    });
});


function findByEmail( email, fn) {
    User.findOneByEmail( email,
        function (err, user) {
            if ( err )
                return fn(null, null);
            else
                return fn(null, user);
        });
}


passport.use(new FacebookStrategy({
        clientID: "1464970643815706",
        clientSecret: "ce03d30a3685f46fabca2d1657a884b0",
        callbackURL: "http://localhost:1337/auth/loginFacebook/callback",
        enableProof: false
    }, function (accessToken, refreshToken, profile, done) {

        var email = profile.emails[0].value;
        findByEmail(email, function (err, user) {
            if (user) {
                user.profilePhoto = "https://graph.facebook.com/" + profile.id + "/picture" + "?width=200&height=200" +
                    "&access_token=" + accessToken;
            }
            // Create a new User if it doesn't exist yet
            if (!user) {
                User.create({
                    email: email
                    // You can also add any other data you are getting back from Facebook here
                    // as long as it is in your model
                }).done(function (err, user) {
                    if (user) {
                        return done(null, user, {
                            message: 'Logged In Successfully'
                        });
                    } else {
                        return done(err, null, {
                            message: 'There was an error logging you in with Facebook'
                        });
                    }
                });

                // If there is already a user, return it
            } else {
                return done(null, user, {
                    message: 'Logged In Successfully'
                });
            }
        });
    }
));