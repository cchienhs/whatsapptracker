#include <QtMath>
#include <QMessageAuthenticationCode>
#include "mathhelper.h"
#include "cryptohelper.h"
#include "loghelper.h"

namespace whatsapptracker{

//Password-Based Key Derivation Function 2
QByteArray CryptoHelper::pbkdf2(const QCryptographicHash::Algorithm& algorithm, const QString& password, const QString& salt, unsigned int count, unsigned int keyLength, bool rawOutput)
{
	if (count <= 0 || keyLength <= 0) {
		LogHelper::log("CryptoHelper:pbkdf2 - Invalid parameters.");
		return QByteArray();
	}

	unsigned int hashLength = QCryptographicHash::hash(QByteArray(), algorithm).length();
	unsigned int blockCount = qCeil(keyLength / hashLength);
	
	QByteArray output;
	QByteArray last;
	QByteArray xorsum;

	for (unsigned long i = 1; i <= blockCount; i++) {
		last = salt.toLatin1() + MathHelper::uLongToQByteArray(i);
		last = xorsum = QMessageAuthenticationCode::hash(last, QByteArray::fromBase64(password.toLatin1()), algorithm );
		for (unsigned int j = 1; j<count; j++) {
			last = QMessageAuthenticationCode::hash(last, QByteArray::fromBase64( password.toLatin1()), algorithm);
			for (unsigned int j = 0; j < last.length(); ++j)
				xorsum[j] = xorsum[j] ^ last[j];
		}
		output += xorsum;
	}

	if (rawOutput) {
		return output.left(keyLength);
	}
	else {
		return output.left(keyLength).toHex();
	}
	
}

}//namespace