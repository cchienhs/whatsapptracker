#include "trackednumber.h"

namespace whatsapptracker {

struct TrackedNumberPrivate
{
public:
    QString phoneNumber;
    QString profilePictureUrl;
    QString id;
};

TrackedNumber::TrackedNumber(QObject* parent)
    :QObject(parent),
    d(new TrackedNumberPrivate)
{

}

TrackedNumber::TrackedNumber(const QString& phoneNumber, const QString& url, const QString& id, QObject* parent)
    : QObject(parent),
    d(new TrackedNumberPrivate)
{
    d->phoneNumber = phoneNumber;
    d->profilePictureUrl = url;
    d->id = id;
}

TrackedNumber::TrackedNumber(const TrackedNumber& rhs)
    : d(new TrackedNumberPrivate)
{
    d->phoneNumber = rhs.phoneNumber();
    d->profilePictureUrl = rhs.profilePictureUrl();
    d->id = rhs.id();
}

TrackedNumber::~TrackedNumber()
{
    delete d;
}

TrackedNumber& TrackedNumber::operator=(const TrackedNumber& rhs)
{
    d->phoneNumber = rhs.phoneNumber();
    d->profilePictureUrl = rhs.profilePictureUrl();
    d->id = rhs.id();
    return *this;
}

void TrackedNumber::setPhoneNumber(const QString& value)
{
    d->phoneNumber = value;
}

QString TrackedNumber::phoneNumber() const
{
    return d->phoneNumber;
}

void TrackedNumber::setProfilePictureUrl(const QString& value)
{
    d->profilePictureUrl = value;
}

QString TrackedNumber::profilePictureUrl() const
{
    return d->profilePictureUrl;
}

void TrackedNumber::setId(const QString& value)
{
    d->id = value;
}

QString TrackedNumber::id() const
{
    return d->id;
}
}