#ifndef WHATSAPPTRACKER_RC4_H
#define WHATSAPPTRACKER_RC4_H

#include <QByteArray>
#include <QString>

namespace whatsapptracker {

struct RC4Private;
class RC4
{
public:
	RC4(const QByteArray& key, unsigned int drop);
	QByteArray cipher(const QByteArray& data, unsigned int offset, unsigned int length);

protected:
	void swap(unsigned char i, unsigned char j);

private:
	RC4Private *d;
};
}
#endif