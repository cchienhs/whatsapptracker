#ifndef WHATSAPPTRACKER_BINTREENODEWRITER_H
#define WHATSAPPTRACKER_BINTREENODEWRITER_H

#include <QMap>
#include <QString>

namespace whatsapptracker {

class KeyStream;
class ProtocolNode;
struct BinTreeNodeWriterPrivate;

class BinTreeNodeWriter
{
public:
	BinTreeNodeWriter();
	~BinTreeNodeWriter();

	void resetKey();
	void setKey(KeyStream* key);
	QString startStream(const QString& domain, const QString& resource);
	QString write(const ProtocolNode* node, bool encrypt = true);

protected:
	void writeAttributes(QMap<QString, QString> attributes);
	void writeString(const QString& tag);
	void writeJid(const QString& user, const QString& server);
	void writeBytes(const QByteArray& bytes);
	void writeToken(int token);
	unsigned int parseInt24(const QString& data);
	QString flushBuffer(bool encrypt = true);
	void writeInternal(const ProtocolNode *node);
	QString writeInt8(int v);
	QString writeInt16(int v);
	QString writeInt24(int v);
	QString getInt24(int length);
	void writeListStart(int len);

private:
	BinTreeNodeWriterPrivate *d;
};
}
#endif