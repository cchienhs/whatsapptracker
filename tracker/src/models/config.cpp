#include "config.h"

namespace whatsapptracker{

struct ConfigPrivate
{
    QString phoneNumber;
    QString secret;
    QString nickName;
    unsigned int databaseLookupRefreshRate = 0;
};

Config::Config(QObject* parent)
    : QObject( parent ),
    d( new ConfigPrivate)
{
}

Config::Config(const QString& phoneNumber, const QString& secret, const QString& nickName, const unsigned int databaseLookupRefreshRate, QObject* parent)
    : QObject( parent ),
    d( new ConfigPrivate )
{
    d->phoneNumber = phoneNumber;
    d->secret = secret;
    d->nickName = nickName;
    d->databaseLookupRefreshRate = databaseLookupRefreshRate;
}

Config::Config(const Config& rhs)
    : d( new ConfigPrivate )
{
    d->phoneNumber = rhs.phoneNumber();
    d->secret = rhs.secret();
    d->nickName = rhs.nickName();
    d->databaseLookupRefreshRate = rhs.databaseLookupRefreshRate();
}

Config& Config::operator=(const Config& rhs)
{
    d->phoneNumber = rhs.phoneNumber();
    d->secret = rhs.secret();
    d->nickName = rhs.nickName();
    d->databaseLookupRefreshRate = rhs.databaseLookupRefreshRate();
    return *this;
}

Config::~Config()
{
    delete d;
}

void Config::setPhoneNumber(const QString& value)
{
    d->phoneNumber = value;
}

QString Config::phoneNumber() const
{
    return d->phoneNumber;
}

void Config::setSecret(const QString& value)
{
    d->secret = value;
}

QString Config::secret() const
{
    return d->secret;
}

void Config::setNickName(const QString& value)
{
    d->nickName = value;
}

QString Config::nickName() const
{
    return d->nickName;
}

void Config::setDatabaseLookupRefreshRate(const unsigned int value)
{
    d->databaseLookupRefreshRate = value;
}

unsigned int Config::databaseLookupRefreshRate() const
{
    return d->databaseLookupRefreshRate;
}

}