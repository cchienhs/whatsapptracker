#ifndef WHATSAPPTRACKER_MATHHELPER_H
#define WHATSAPPTRACKER_MATHHELPER_H

#include <QByteArray>

namespace whatsapptracker{

class MathHelper
{
public:
	static int randInt(int low, int high);
	static QByteArray uLongToQByteArray(unsigned long);

};
}
#endif