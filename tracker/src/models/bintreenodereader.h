#ifndef WHATSAPPTRACKER_BINTREENODEREADER_H
#define WHATSAPPTRACKER_BINTREENODEREADER_H

#include <QMap>
#include <QString>

namespace whatsapptracker{

class ProtocolNode;
struct BinTreeNodeReaderPrivate;
class KeyStream;
class BinTreeNodeReader
{
public:
	BinTreeNodeReader();
	~BinTreeNodeReader();

	ProtocolNode* nextTree(const QByteArray& input);
	void resetKey();
	void setKey(KeyStream* key);

protected:
	QMap<QString, QString> readAttributes(unsigned int size);
	QString readNibble();
	QString getToken(int token);
	QByteArray readString(unsigned char token);
	bool isListTag(unsigned char token);
	ProtocolNode* nextTreeInternal();
	unsigned int peekInt24(unsigned int offset = 0);
	unsigned int readInt24();
	unsigned int peekInt16(unsigned int offset=0);
	unsigned int readInt16();
	unsigned int peekInt8(unsigned int offset = 0);
	unsigned int readInt8();
	QByteArray fillArray(unsigned int len);
	QList<ProtocolNode*> readList(unsigned char token);
	unsigned int readListSize(unsigned char token);

private:
	BinTreeNodeReaderPrivate *d;
};
}
#endif