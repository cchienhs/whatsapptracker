#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "presencetaghandler.h"

namespace whatsapptracker {

void PresenceTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	if (node->getAttribute("status").compare("dirty") == 0) {
		//clear dirty
		QStringList categories;
		if (node->getChildren().length() > 0) {
			foreach(ProtocolNode* child, node->getChildren()) {
				if (child->getTag().compare("category") == 0) {
					categories.append(child->getAttribute("name"));
				}
			}
		}
		controller->sendClearDirty(categories);
	}

	if (controller->phoneNumber().compare(node->getAttribute("from").left(controller->phoneNumber().length())) != 0
		&& node->getAttribute("from").indexOf("-") < 0) {

		if (node->getAttribute("type").isEmpty()) {
			controller->onPresenceAvailable(controller->phoneNumber(), node->getAttribute("from"));
		}
		else {
			controller->onPresenceUnavailable(controller->phoneNumber(), node->getAttribute("from"), node->getAttribute("last"));
		}
	}

	if (controller->phoneNumber().compare(node->getAttribute("from").left(controller->phoneNumber().length())) != 0
		&& node->getAttribute("from").indexOf("-") < 0
		&& !node->getAttribute("type").isEmpty()) {

		QString groupId = node->getAttribute("from");
		if (!node->getAttribute("add").isEmpty()) {
			controller->onGroupsParticipantsAdd(controller->phoneNumber(), groupId, node->getAttribute("add"));
		}
		else if (!node->getAttribute("remove").isEmpty()) {
			controller->onGroupsParticipantsRemove(controller->phoneNumber(), groupId, node->getAttribute("remove"));
		}
	}
}
}