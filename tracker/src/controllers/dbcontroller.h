#ifndef WHATSAPPTRACKER_DBCONTROLLER_H
#define WHATSAPPTRACKER_DBCONTROLLER_H

#include <QList>
#include <QString>
#include <QThread>

#include "../models/user.h"
#include "../models/config.h"
#include "../models/trackednumber.h"

namespace whatsapptracker {

//callback class signature
class GetUsersCallback
{
public:
    virtual void onGetUsers( const QList<User>& users ) = NULL;
};

class UpdateUserCallback
{
public:
    virtual void onUpdateUser(const bool status) = NULL;
};

class GetConfigCallback
{
public:
    virtual void onGetConfig(const Config& config) = NULL;
};

class UpdateConfigCallback
{
public:
    virtual void onUpdateConfig(const bool) = NULL;
};

struct DbControllerPrivate;
class DbController : public QThread
{
    Q_OBJECT

public:
    enum DatabaseType{ mongo=0 };
	DbController( QObject* parent=NULL );
	~DbController();

	void setup(const QString& url, const QString& dbName, const QString& username, const QString& password, DatabaseType dbType );
    
    //users management
    void getUsers( GetUsersCallback* callback );
    void updateUser(const User& user, UpdateUserCallback* callback );
    
    //config management
    void getConfig(GetConfigCallback* callback);
    void updateConfig(const Config& config, UpdateConfigCallback* callback);

    //tracked numbers management
    /*void insertTrackedNumber(const TrackedNumber& TrackedNumber);
    void updateTrackedNumber(const TrackedNumber& TrackedNumber);
    void deleteTrackedNumber(const QString& phoneNumber);
    void getTrackedNumber(const QString& phoneNumber);*/


protected:
    //the invokable methods invoked by their corresponding public methods. These are required so that QTimer can be used when the service is invoked when worker class has not been instantiated.
    Q_INVOKABLE void getUsers();
    Q_INVOKABLE void updateUser();
    Q_INVOKABLE void getConfig();
    Q_INVOKABLE void updateConfig();
    void run();

signals:
    //these signals are emitted internally to the worker objects. External classes should not be using these.
    void getUsersSignal();
    void updateUserSignal(const User& user);
    void getConfigSignal();
    void updateConfigSignal(const Config& config);

private:
    //these private slots are invoked when worker classes have completed their jobs
    Q_INVOKABLE void onGetUsers(const QList<User>& users);
    Q_INVOKABLE void onUpdateUser(const bool status);
    Q_INVOKABLE void onGetConfig(const Config& config);
    Q_INVOKABLE void onUpdateConfig(const bool status);
    DbControllerPrivate *d;
};
}
#endif