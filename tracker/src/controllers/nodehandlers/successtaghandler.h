#ifndef WHATSAPPTRACKER_SUCCESSTAGHANDLER_H
#define WHATSAPPTRACKER_SUCCESSTAGHANDLER_H

#include "nodehandler.h"

namespace whatsapptracker {
class SuccessTagHandler
{
public:
	void handle(WhatsAppController *controller, ProtocolNode* node, const bool autoReceipt, const QString& type);
};
}

#endif