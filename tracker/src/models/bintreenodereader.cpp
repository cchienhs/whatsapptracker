#include "../helpers/loghelper.h"
#include "tokenmap.h"
#include "keystream.h"
#include "bintreenodereader.h"
#include "protocolnode.h"

namespace whatsapptracker {

struct BinTreeNodeReaderPrivate
{
	QByteArray input;
	KeyStream *key = NULL;
};

BinTreeNodeReader::BinTreeNodeReader()
	:d(new BinTreeNodeReaderPrivate)
{

}

BinTreeNodeReader::~BinTreeNodeReader()
{
	delete d;
}

void BinTreeNodeReader::resetKey()
{
	d->key = NULL;
}

void BinTreeNodeReader::setKey(KeyStream *key)
{
	d->key = key;
}

ProtocolNode* BinTreeNodeReader::nextTree( const QByteArray& input )
{
	if (!input.isEmpty()) {
		d->input = QByteArray(input.constData(), input.length());
	}
	unsigned int firstByte = this->peekInt8();
	unsigned int stanzaFlag = ((unsigned char)firstByte & 0xF0) >> 4;
	unsigned int stanzaSize = this->peekInt16(1) | (((unsigned char)firstByte & 0x0F) << 16);
	if (stanzaSize > d->input.length()) {
		LogHelper::log("Incomplete message $stanzaSize != " + QString::number(d->input.length()) );
	}
	this->readInt24();
	if (stanzaFlag & 8) {
		if (d->key != NULL) {
			unsigned int realSize = stanzaSize - 4;
			d->input = d->key->decodeMessage(d->input, realSize, 0, realSize);// . $remainingData;
		}
		else {
			LogHelper::log("Encountered encrypted message, missing key");
		}
	}
	if (stanzaSize > 0) {
		return this->nextTreeInternal();
	}

	return NULL;
}

unsigned int BinTreeNodeReader::peekInt16( unsigned int offset )
{
	unsigned int ret = 0;
	if (d->input.length() >= (offset + 2)) {
		ret = (unsigned char)d->input[offset] << 8;
		ret |= (unsigned char)d->input[offset + 1] << 0;
	}

	return ret;
}

unsigned int BinTreeNodeReader::readInt16()
{
	unsigned int ret = this->peekInt16();
	if (ret > 0) {
		d->input = d->input.right( d->input.length() - 2);
	}

	return ret;
}

unsigned int BinTreeNodeReader::peekInt8( unsigned int offset )
{
	unsigned int ret = 0;
	if (d->input.length() >= (1 + offset)) {
		unsigned char sbstr = d->input[offset];
		ret = sbstr;
	}

	return ret;
}

unsigned int BinTreeNodeReader::readInt8()
{
	unsigned int ret = this->peekInt8();
	if (d->input.length() >= 1) {
		d->input = d->input.right(d->input.length() - 1);
	}

	return ret;
}

unsigned int BinTreeNodeReader::peekInt24( unsigned int offset )
{
	unsigned int ret = 0;
	if (d->input.length() >= (3 + offset)) {
		ret = (unsigned char)d->input[offset] << 16;
		ret |= (unsigned char)d->input[offset+1] << 8;
		ret |= (unsigned char)d->input[offset+2] << 0;
	}

	return ret;
}

unsigned int BinTreeNodeReader::readInt24()
{
	unsigned int ret = this->peekInt24();
	if (d->input.length() >= 3) {
		d->input = d->input.right(d->input.length() - 3);
	}
	return ret;
}

QByteArray BinTreeNodeReader::fillArray( unsigned int len )
{
	QByteArray ret;
	if (d->input.length() >= len) {
		ret = d->input.left(len);
		d->input = d->input.right(d->input.length() - len);
	}
	return ret;
}

unsigned int BinTreeNodeReader::readListSize(unsigned char token)
{
	if (token == 0xf8) {
		return this->readInt8();
	}
	else if (token == 0xf9) {
		return this->readInt16();
	}
	else {
		LogHelper::log("BinTreeNodeReader->readListSize: Invalid token $token");
		return 0;
	}
}

QList<ProtocolNode*> BinTreeNodeReader::readList( unsigned char token )
{
	int size = this->readListSize(token);
	QList<ProtocolNode*> ret;
	for (int i = 0; i<size; i++) {
		ret.append(this->nextTreeInternal());
	}
	return ret;
}

QString BinTreeNodeReader::readNibble() 
{
	unsigned char byte = this->readInt8();
	bool ignoreLastNibble = (bool)(byte & 0x80);
	
	unsigned int size = (byte & 0x7f);
	unsigned int nrOfNibbles = size * 2 - (int)ignoreLastNibble;

	QString data = QString::fromLatin1( this->fillArray(size) );
	QString string;

	for (unsigned int i = 0; i < nrOfNibbles; i++) {
		unsigned char ord = data[(int)floor(i / 2)].toLatin1();
		
		unsigned int shift = 4 * (1 - i % 2);
		unsigned int decimal = (ord & (15 << shift)) >> shift;

		switch (decimal) {
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			string += QChar(decimal);
			break;
		case 10:
		case 11:
			string += QChar(decimal - 10 + 45);
			break;
		default:
			LogHelper::log("Bad nibble: $decimal");
		}
	}

	return string;
}

QString BinTreeNodeReader::getToken( int token )
{
	QString ret;
	bool subdict = false;
	TokenMap::getToken( token, subdict, ret );
	if (ret.isEmpty()) {
		token = this->readInt8();
		TokenMap::getToken(token, subdict, ret);
		if (ret.isEmpty()) {
			LogHelper::log("BinTreeNodeReader->getToken: Invalid token $token");
		}
	}
	return ret;
}

QByteArray BinTreeNodeReader::readString( unsigned char token )
{
	QByteArray ret;

	if ((token > 2) && (token < 0xf5)) {
		ret = this->getToken(token).toLatin1();
	} 
	else if(token == 0) {
		//do nothing
	} 
	else if(token == 0xfc) {
		unsigned int size = this->readInt8();
		ret = this->fillArray(size);
	} 
	else if(token == 0xfd) {
		unsigned int size = this->readInt24();
		ret = this->fillArray(size);
	} 
	else if(token == 0xfa) {
		QString user = QString::fromLatin1( this->readString(this->readInt8()));
		QString server = QString::fromLatin1( this->readString(this->readInt8()));
		if ((user.length() > 0) && (server.length() > 0)) {
			ret = QString( user + "@" + server ).toLatin1();
		} 
		else if(server.length() > 0) {
			ret = server.toLatin1();
		}
	} 
	else if(token == 0xff) {
		ret = this->readNibble().toLatin1();
	}

	return ret;
}

QMap<QString,QString> BinTreeNodeReader::readAttributes( unsigned int size )
{
	QMap<QString, QString> attributes;
	unsigned int attribCount = (size - 2 + size % 2) / 2;

	for (unsigned int i = 0; i<attribCount; i++) {
		QString key = this->readString(this->readInt8());
		QString value = this->readString(this->readInt8());
		attributes[key] = value;
	}

	return attributes;
}

bool BinTreeNodeReader::isListTag( unsigned char token )
{
	return (token == 248 || token == 0 || token == 249);
}

ProtocolNode* BinTreeNodeReader::nextTreeInternal()
{
	unsigned int token = this->readInt8();
	unsigned int size = this->readListSize(token);
	token = this->readInt8();
	if (token == 1) {
		QMap<QString,QString> attributes = this->readAttributes(size);
		QString input("start");
		return new ProtocolNode(input, attributes, QList<ProtocolNode*>(), QByteArray());
	} 
	else if(token == 2) {
		return NULL;
	}

	QString tag = this->readString(token);
	QMap<QString,QString> attributes = this->readAttributes(size);
	if ((size % 2) == 1) {
		return new ProtocolNode(tag, attributes, QList<ProtocolNode*>(), QByteArray());
	}
	token = this->readInt8();
	if (this->isListTag(token)) {
		return new ProtocolNode(tag, attributes, this->readList(token), QByteArray());
	}

	return new whatsapptracker::ProtocolNode(tag, attributes, QList<ProtocolNode*>(), this->readString(token));
}
}//namespace

