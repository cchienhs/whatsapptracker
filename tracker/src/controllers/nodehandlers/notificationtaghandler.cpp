#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "../../helpers/loghelper.h"
#include "notificationtaghandler.h"

namespace whatsapptracker {

void NotificationTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	QString name = node->getAttribute("notify");
	QString typeTag = node->getAttribute("type");
	
	if (typeTag.compare("status") == 0) {
		controller->onGetStatus( controller->phoneNumber(), //my number
			node->getAttribute("from"),
			node->getChild(0)->getTag(),
			node->getAttribute("id"),
			node->getAttribute("t"),
			node->getChild(0)->getData());
	}
	else if (typeTag.compare("picture") == 0) {
		if (node->hasChild("set")) {
			controller->onProfilePictureChanged( controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("t"));
		}
		else if (node->hasChild("delete")) {
			controller->onProfilePictureDeleted(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				node->getAttribute("t"));
		}
		//TODO
	}
	else if (typeTag.compare("contacts") == 0) {
		QString notification = node->getChild(0)->getTag();
		if (notification.compare("add") == 0) {
			controller->onNumberWasAdded(controller->phoneNumber(), node->getChild(0)->getAttribute("jid"));
		}
		else if(notification.compare("remove") == 0) {
			controller->onNumberWasRemoved(controller->phoneNumber(), node->getChild(0)->getAttribute("jid"));
		}
		else if(notification.compare("update") == 0) {
			controller->onNumberWasUpdated(controller->phoneNumber(), node->getChild(0)->getAttribute("jid"));
		}
	}
	else if (typeTag.compare("encrypt") == 0) {
		QString value = node->getChild(0)->getAttribute("value");
		bool isNum = false;
		value.toInt(&isNum);
		if (isNum) {
			controller->onGetKeysLeft(controller->phoneNumber(), node->getChild(0)->getAttribute("value"));
		}
		else {
			LogHelper::log( "Corrupt Stream: value " + value + "is not numeric");
		}
	}
	else if (typeTag.compare("w:gp2") == 0) {
		if (node->hasChild("remove")) {
			if (node->getChild(0)->hasChild("participant"))
				controller->onGroupsParticipantsRemove(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getChild(0)->getChild(0)->getAttribute("jid"));
		}
		else if (node->hasChild("add")) {
			controller->onGroupsParticipantsAdd(controller->phoneNumber(),
				node->getAttribute("from"),
				node->getChild(0)->getChild(0)->getAttribute("jid"));
		}
		else if (node->hasChild("create")) {
			QStringList groupMembers;
			foreach( ProtocolNode* cn, node->getChild(0)->getChild(0)->getChildren()) {
				groupMembers.append( cn->getAttribute("jid") );
			}
			controller->onGroupisCreated(
				controller->phoneNumber(),
				node->getChild(0)->getChild(0)->getAttribute("creator"),
				node->getChild(0)->getChild(0)->getAttribute("id"),
				node->getChild(0)->getChild(0)->getAttribute("subject"),
				node->getAttribute("participant"),
				node->getChild(0)->getChild(0)->getAttribute("creation"),
				groupMembers);
		}
		else if (node->hasChild("subject")) {
			controller->onGetGroupsSubject(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("t"),
				node->getAttribute("participant"),
				node->getAttribute("notify"),
				node->getChild(0)->getAttribute("subject"));
		}
		else if (node->hasChild("promote")) {
			QStringList promotedJIDs;
			foreach( ProtocolNode* cn, node->getChild(0)->getChildren() ) {
				promotedJIDs.append(cn->getAttribute("jid"));
			}
			controller->onGroupsParticipantsPromote(
				controller->phoneNumber(),
				node->getAttribute("from"),        //Group-JID
				node->getAttribute("t"),           //Time
				node->getAttribute("participant"), //Issuer-JID
				node->getAttribute("notify"),      //Issuer-Name
				promotedJIDs);
		}
	}
	else if (typeTag.compare("account") == 0) {
		QString author;

		if ((node->getChild(0)->getAttribute("author")).isEmpty())
			author = "Paypal";
		else
			author = node->getChild(0)->getAttribute("author");
		controller->onPaidAccount(
			controller->phoneNumber(),
			author,
			node->getChild(0)->getChild(0)->getAttribute("kind"),
			node->getChild(0)->getChild(0)->getAttribute("status"),
			node->getChild(0)->getChild(0)->getAttribute("creation"),
			node->getChild(0)->getChild(0)->getAttribute("expiration"));
	}
	else if (typeTag.compare("features") == 0) {
		if (node->getChild(0)->getChild(0)->getTag().compare("encrypt") == 0 ) {
			controller->onGetFeature(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getChild(0)->getChild(0)->getAttribute("value"));
		}
	}
	else if (typeTag.compare("web") == 0) {
		if ((node->getChild(0)->getTag().compare("action") == 0 ) && (node->getChild(0)->getAttribute("type").compare("sync") == 0 ))
		{
			QList<ProtocolNode*> data = node->getChild(0)->getChildren();
			controller->onWebSync(
				controller->phoneNumber(),
				node->getAttribute("from"),
				node->getAttribute("id"),
				data[0]->getData(),
				data[1]->getData(),
				data[2]->getData());
		}
	}
	else {
		LogHelper::log( "Method $type not implemented" );
	}

	controller->sendAck(node, "notification");
}
}
