#-------------------------------------------------
#
# Project created by QtCreator 2015-06-16T22:50:18
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = tracker
CONFIG   += console
CONFIG   -= app_bundle

DESTDIR = bin
MOC_DIR += ./generatedfiles
OBJECTS_DIR += ./generatedfiles
UI_DIR += ./generatedfiles
RCC_DIR += ./generatedfiles
TEMPLATE = app
DEFINES += STATIC_LIBMONGOCLIENT

INCLUDEPATH += libraries/mongo/include \
	libraries/boost/include

win32 {
	LIBS += -L"./libraries/boost/lib/msvc12/" \
			-L"./libraries/mongo/lib/win32"
}

CONFIG(debug, debug|release) {
	#LIBS += ./libraries/mongo/lib/mongoclient-gd.lib
} 
else {
	#LIBS += ./libraries/mongo/lib/mongoclient.lib
}

SOURCES += src/constants.cpp \
	src/controllers/nodehandlers/acktaghandler.cpp \
	src/controllers/nodehandlers/calltaghandler.cpp \
	src/controllers/nodehandlers/chatstatetaghandler.cpp \
	src/controllers/nodehandlers/ibtaghandler.cpp \
	src/controllers/nodehandlers/iqtaghandler.cpp \
	src/controllers/nodehandlers/messaagetaghandler.cpp \
	src/controllers/nodehandlers/notificationtaghandler.cpp \
	src/controllers/nodehandlers/presencetaghandler.cpp \
	src/controllers/nodehandlers/receipttaghandler.cpp \
	src/controllers/nodehandlers/successtaghandler.cpp \
	src/controllers/db/mongoworker.cpp \
	src/controllers/dbcontroller.cpp \
	src/controllers/whatsappcontroller.cpp \
	src/helpers/cryptohelper.cpp \
	src/helpers/filesystemhelper.cpp \
	src/helpers/loghelper.cpp \
	src/helpers/mathhelper.cpp \
	src/helpers/whatsapphelper.cpp \
	src/main.cpp \
	src/models/bintreenodereader.cpp \
	src/models/bintreenodewriter.cpp \
	src/models/keystream.cpp \
	src/models/protocolnode.cpp \
	src/models/rc4.cpp \
	src/models/tokenmap.cpp \
	src/models/user.cpp \
	src/models/config.cpp \
	src/models/trackednumber.cpp

HEADERS += \
    src/constants.h \
	src/controllers/nodehandlers/acktaghandler.h \
	src/controllers/nodehandlers/calltaghandler.h \
	src/controllers/nodehandlers/chatstatetaghandler.h \
	src/controllers/nodehandlers/ibtaghandler.h \
	src/controllers/nodehandlers/iqtaghandler.h \
	src/controllers/nodehandlers/messagetaghandler.h \
	src/controllers/nodehandlers/nodehandler.h \
	src/controllers/nodehandlers/notificationtaghandler.h \
	src/controllers/nodehandlers/presencetaghandler.h \
	src/controllers/nodehandlers/receipttaghandler.h \
	src/controllers/nodehandlers/successtaghandler.h \
	src/controllers/db/dbworkerinterface.h \
	src/controllers/db/mongoworker.h \
	src/controllers/dbcontroller.h \
	src/controllers/whatsappcontroller.h \
	src/helpers/cryptohelper.h \
	src/helpers/filesystemhelper.h \
	src/helpers/loghelper.h \
	src/helpers/mathhelper.h \
	src/helpers/whatsapphelper.h \
	src/models/bintreenodereader.h \
	src/models/bintreenodewriter.h \
	src/models/keystream.h \
	src/models/messagestoreinterface.h \
	src/models/protocolnode.h \
	src/models/rc4.h \
	src/models/syncresult.h \
	src/models/tokenmap.h \
	src/models/user.h \
	src/models/config.h \
	src/models/trackednumber.h