#include <QString>
#include "loghelper.h"

namespace whatsapptracker {

void LogHelper::log(const QString& content)
{
	qDebug( content.toLatin1() );
}
}//namespace