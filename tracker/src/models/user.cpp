#include "user.h"

namespace whatsapptracker {

struct UserPrivate
{
    UserPrivate() {
    }
    QString email;
    QString phoneNumber;
    QMap<QString, QPair<QString,bool>> phoneNumbersToTrack;
    bool paidMember;
    QString id;
};

User::User( QObject *parent )
    : QObject( parent ),
      d( new UserPrivate )
{
 
}

User::User(const QString& email, const QString& phoneNumber, const QMap<QString,QPair<QString, bool>>& phoneNumbersToTrack, const bool paidMember, const QString& id, QObject *parent)
    : QObject(parent),
      d(new UserPrivate)
{
    d->email = email;
    d->phoneNumber = phoneNumber;
    d->phoneNumbersToTrack = phoneNumbersToTrack;
    d->paidMember = paidMember;
    d->id = id;
}

User::User(const User& rhs)
    : d(new UserPrivate)
{
    d->email = rhs.email();
    d->phoneNumbersToTrack = rhs.phoneNumbersToTrack();
    d->phoneNumber = rhs.phoneNumber();
    d->paidMember = rhs.paidMember();
    d->id = rhs.id();
}

User& User::operator=(const User& rhs)
{
    d->email = rhs.email();
    d->phoneNumbersToTrack = rhs.phoneNumbersToTrack();
    d->phoneNumber = rhs.phoneNumber();
    d->paidMember = rhs.paidMember();
    d->id = rhs.id();
    return *this;
}

User::~User()
{
    delete d;
}

void User::setEmail(const QString& value)
{
    d->email = value;
}

QString User::email() const
{
    return d->email;
}

void User::setPhoneNumber(const QString& value)
{
    d->phoneNumber = value;
}

QString User::phoneNumber() const
{
    return d->phoneNumber;
}

void User::setPhoneNumbersToTrack(const QMap<QString, QPair<QString,bool>>& value)
{
    d->phoneNumbersToTrack = value;
}

QMap<QString, QPair<QString,bool>> User::phoneNumbersToTrack() const
{
    return d->phoneNumbersToTrack;
}

void User::setPaidMember(const bool value)
{
    d->paidMember = value;
}

bool User::paidMember()const
{
    return d->paidMember;
}

void User::setId(const QString& value)
{
    d->id = value;
}

QString User::id() const
{
    return d->id;
}
}