/**
 * MainController module
 * Responsible for initializing the global variables and passing them to other controllers using session cookie as a medium
 */

angular.module('whatsAppTrackerApp').controller('userController', ['$scope','$http','$cookies', '$modal',
    function($scope,$http, $cookies,$modal) {

        $scope.changedDataUpdatedToServer = true;
        $scope.animationsEnabled = true;

        $scope.updateCookie = function() {
            $cookies.putObject("user", $scope.user, []);
        };

        $scope.noOfActiveTrackedNumbers = function( ) {
            var count = 0;
            angular.forEach( $scope.user.phoneNumbersToTrack,
                function (item) {
                    if (item.track)
                        { count++ }
                });
            return count;
        };

        $scope.noOfTrackedNumbers = function() {
            return $scope.user.phoneNumbersToTrack.length;
        };

        $scope.updateUserToDatabase = function( user ) {
            if( !$scope.changedDataUpdatedToServer ) {
                $scope.user = user;
                $http.put('/user/' + user.email, {user: user}).
                    success(function (data, status, headers, config) {
                        if (data == "Success")
                            $scope.changedDataUpdatedToServer = true;
                        else if (data == "Error")
                            alert("Error in Updating to DB. Please try again later.");
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
            }
        };

        $scope.editTrackedNumber = function( name, size ) {
            var editItem;
            for( var i = 0; i < $scope.user.phoneNumbersToTrack.length; ++i ) {
                if( $scope.user.phoneNumbersToTrack[i].phoneNumber === name ) {
                    editItem = $scope.user.phoneNumbersToTrack[i];
                    break;
                }
            }

            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/app/partials/dialogs/editnumber.html',
                controller: 'editNumberDialogController',
                size: size,
                resolve: {
                    editNumber: function () {
                        return editItem;
                    }
                }
            });

            modalInstance.result.then(function (editNumber) {
                for( var i = 0; i < $scope.user.phoneNumbersToTrack.length; ++i ) {
                    if( $scope.user.phoneNumbersToTrack[i].phoneNumber === editNumber.phoneNumber ) {
                        $scope.user.phoneNumbersToTrack[i].nickName = editNumber.nickName;
                        $scope.user.phoneNumbersToTrack[i].track = editNumber.track;
                        $scope.changedDataUpdatedToServer = false;
                        break;
                    }
                }
            }, function () {
            });
        };

        $scope.removeTrackedNumber = function( name ) {
            var index = -1;
            for( var i = 0; i < $scope.user.phoneNumbersToTrack.length; ++i ) {
                if( $scope.user.phoneNumbersToTrack[i].phoneNumber === name ) {
                    index = i;
                    break;
                }
            }
            if( index >= 0 ) {
                $scope.changedDataUpdatedToServer = false;
                $scope.user.phoneNumbersToTrack.splice(index, 1);
            }
        };

        $scope.addTrackedNumber = function( size ) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: '/app/partials/dialogs/addnumber.html',
                controller: 'addNumberDialogController',
                size: size
            });

            modalInstance.result.then(function (newNumber) {
                $scope.user.phoneNumbersToTrack.push(newNumber);
                $scope.changedDataUpdatedToServer = false;
            }, function () {
            });

        };
}]);


