#ifndef WHATSAPPTRACKER_USER_H
#define WHATSAPPTRACKER_USER_H

#include <QObject>
#include <QMap>
#include <QMetaType>
#include <QPair>
#include <QString>

namespace whatsapptracker {

struct UserPrivate;
class User : public QObject
{
public:
    User( QObject* parent=NULL);
    User(const QString& username, const QString& phoneNumber, const QMap<QString,QPair<QString,bool>>& phoneNumbersToTrack, const bool paidMember, const QString& id=QString(), QObject* parent=NULL);
    User(const User& rhs);
    ~User();

    User& operator=(const User& rhs);

    void setEmail(const QString& value);
    QString email() const;
    void setPhoneNumber(const QString& value);
    QString phoneNumber() const;
    void setPhoneNumbersToTrack(const QMap<QString, QPair<QString,bool>>& value);
    QMap<QString, QPair<QString,bool>> phoneNumbersToTrack() const;
    void setPaidMember(const bool value);
    bool paidMember()const;;
    void setId(const QString& value);
    QString id() const;

private:
    UserPrivate *d;
};
}

Q_DECLARE_METATYPE(whatsapptracker::User)
Q_DECLARE_METATYPE(QList<whatsapptracker::User>)

#endif