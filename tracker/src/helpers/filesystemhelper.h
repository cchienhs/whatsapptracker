#ifndef WHATSAPPTRACKER_FILESYSTEMHELPER_H
#define WHATSAPPTRACKER_FILESYSTEMHELPER_H

#include <QByteArray>
#include <QString>

namespace whatsapptracker {

class FileSystemHelper
{
public:
    static void createFile( const QString& name );
	static bool writeContent(const QString& filename, const QByteArray& content, const bool overwrite = true);
	static QByteArray getContent(const QString& filename);
};
}
#endif
