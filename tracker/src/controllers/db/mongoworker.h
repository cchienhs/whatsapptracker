#ifndef WHATSAPPTRACKER_MONGOWORKER_H
#define WHATSAPPTRACKER_MONGOWORKER_H

#include "dbworkerinterface.h"

namespace whatsapptracker {

class Config;
class User;
struct MongoWorkerPrivate;
class MongoWorker : public DbWorkerInterface
{
    Q_OBJECT

public:
    MongoWorker(QObject* parent = NULL);
    ~MongoWorker();

//reimplemented
    void connect(const QString& url, const QString& dbName, const QString& username, const QString& password);

    //tracked numbers managment
     void insertTrackedNumber(const TrackedNumber& TrackedNumber);
     void updateTrackedNumber(const TrackedNumber& TrackedNumber);
     void deleteTrackedNumber(const QString& phoneNumber);
     void getTrackedNumber(const QString& phoneNumber);


    //users management    
    void getUsers();
    void updateUser(const User& users);
    
    //config management
    void getConfig();
    void updateConfig(const Config& config);

private:
    bool userExists(const User& user);
    MongoWorkerPrivate *d;
};
}
#endif