#include <QDir>
#include <QFile>
#include "filesystemhelper.h"

namespace whatsapptracker {

/**
 * @brief creates the directory and challenge file if it does not esixts yet
 * @param name -pathname with directories separated by '/'
 */
void FileSystemHelper::createFile( const QString& name )
{
    QFile file(name);
    if( !file.exists() ) {
        QStringList directories = name.split( '/' );
        if( directories.length() > 1 ) {
            directories.removeLast();
            QString dirPath = directories.join( '/' );

            QDir dir;
            dir.mkpath( dirPath );
        }

        file.open( QIODevice::WriteOnly );
        file.close();
    }
}

/**
* @brief write content in file specified by filename
* @param filename -file to write to. Full path should be given with directories separated by '/'
* @param content -content in bytes
* @param overwrite -true:overwrite; false:append
*/
bool FileSystemHelper::writeContent(const QString& filename, const QByteArray& content, const bool overwrite)
{
	createFile(filename);
	QFile file(filename);
	if (overwrite) {
		if (file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			file.write(content);
			return true;
		}
	}
	else {
		if (file.open(QIODevice::WriteOnly | QIODevice::Append)) {
			file.write(content);
			return true;
		}
	}	
	return false;
}

QByteArray FileSystemHelper::getContent(const QString& filename)
{
	QByteArray ret;
	QFile file(filename);
	if (file.open(QIODevice::ReadOnly)) {
		ret = file.readAll();
	}
	return ret;
}
}//namespace
