#ifndef WHATSAPPTRACKER_LOGHELPER_H
#define WHATSAPPTRACKER_LOGHELPER_H

class QString;

namespace whatsapptracker {

class LogHelper
{
public:
	static void log(const QString& content);
};
}
#endif