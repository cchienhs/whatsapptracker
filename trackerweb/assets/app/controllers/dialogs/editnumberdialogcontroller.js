
angular.module('whatsAppTrackerApp').controller('editNumberDialogController', function ($scope, $modalInstance, editNumber)
{
    $scope.number = {};
    $scope.number.phoneNumber = editNumber.phoneNumber;
    $scope.number.nickName = editNumber.nickName;
    $scope.number.track = editNumber.track;

    $scope.ok = function () {
        $modalInstance.close($scope.number);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});