#ifndef WHATSAPPTRACKER_NODEHANDLER_H
#define WHATSAPPTRACKER_NODEHANDLER_H

namespace whatsapptracker {

class WhatsAppController;
class ProtocolNode;
class NodeHandler
{
public:
	virtual void handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type) = 0;
};
}
#endif