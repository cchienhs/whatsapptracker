#include "constants.h"

namespace whatsapptracker {
const QString Constants::CONNECTED_STATUS = "connected"; // Describes the connection status with the WhatsApp server.
const QString Constants::DISCONNECTED_STATUS = "disconnected"; // Describes the connection status with the WhatsApp server.
const QString Constants::MEDIA_FOLDER = "media"; // The relative folder to store received media files
const QString Constants::PICTURES_FOLDER = "pictures"; // The relative folder to store picture files
const QString Constants::DATA_FOLDER = "wadata"; // The relative folder to store cache files.
const QString Constants::WHATSAPP_CHECK_HOST = "v.whatsapp.net/v2/exist"; // The check credentials host.
const QString Constants::WHATSAPP_GROUP_SERVER = "g.us"; // The Group server hostname
const QString Constants::WHATSAPP_REGISTER_HOST = "v.whatsapp.net/v2/register"; // The register code host.
const QString Constants::WHATSAPP_REQUEST_HOST = "v.whatsapp.net/v2/code"; // The request code host.
const QString Constants::WHATSAPP_SERVER = "s.whatsapp.net"; // The hostname used to login/send messages.
const QString Constants::WHATSAPP_DEVICE = "S40"; // The device name.
const QString Constants::WHATSAPP_VER = "2.12.81"; // The WhatsApp version.
const QString Constants::WHATSAPP_USER_AGENT = "WhatsApp/2.12.81 S40Version/14.26 Device/Nokia302"; // User agent used in request/registration code.
const QString Constants::WHATSAPP_VER_CHECKER = "https://coderus.openrepos.net/whitesoft/whatsapp_scratch";
}
