#ifndef WHATSAPPTRACKER_TRACKEDNUMBER_H
#define WHATSAPPTRACKER_TRACKEDNUMBER_H

#include <QObject>
#include <QString>

namespace whatsapptracker {

    struct TrackedNumberPrivate;
    class TrackedNumber : public QObject
    {
    public:
        TrackedNumber(QObject* parent = NULL);
        TrackedNumber(const QString& phoneNumber, const QString& url, const QString& id = QString(), QObject* parent = NULL);
        TrackedNumber(const TrackedNumber& rhs);
        ~TrackedNumber();

        TrackedNumber& operator=(const TrackedNumber& rhs);

        void setPhoneNumber(const QString& value);
        QString phoneNumber() const;
        void setProfilePictureUrl(const QString& value);
        QString profilePictureUrl() const;
        void setId(const QString& value);
        QString id() const;

    private:
        TrackedNumberPrivate *d;
    };
}

Q_DECLARE_METATYPE(whatsapptracker::TrackedNumber)

#endif