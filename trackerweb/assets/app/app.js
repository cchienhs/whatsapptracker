'use strict';

/**
 * The main Sails Angular app module
 *
 * @type {angular.Module}
 */
var app = angular.module('whatsAppTrackerApp', ['ngCookies','ui.bootstrap']);
