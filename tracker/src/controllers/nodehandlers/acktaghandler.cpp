#include "../whatsappcontroller.h"
#include "../../models/protocolnode.h"
#include "acktaghandler.h"

namespace whatsapptracker {

void AckTagHandler::handle(WhatsAppController* controller, ProtocolNode* node, const bool autoReceipt, const QString& type)
{
	controller->onMessageReceivedServer(controller->phoneNumber(), node->getAttribute("from"),
		node->getAttribute("id"), node->getAttribute("class"),
		node->getAttribute("t"));
}
}