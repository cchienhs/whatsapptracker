#ifndef WHATSAPPTRACKER_WHATSAPPCONTROLLER_H
#define WHATSAPPTRACKER_WHATSAPPCONTROLLER_H

#include <QAbstractSocket>
#include <QByteArray>
#include <QStringList>
#include <QQueue>
#include <QObject>
class QDateTime;

namespace whatsapptracker {

class MessageStoreInterface;
class KeyStream;
class BinTreeNodeWriter;
class SyncResult;
class ProtocolNode;
struct WhatsAppControllerPrivate;

class WhatsAppController :public QObject
{
	Q_OBJECT

public:
    WhatsAppController( const QString& number="972546267207", const QString& secret="UZxjUXy83QQhVLLgqw/ishTwTzE=", const QString& nickname = "WhatsApp", const bool debug=false, QObject* parent=NULL );
	void connect();
	bool isConnected();
	void login();
	bool isLoggedIn();
	
	QByteArray readStanza();
	void sendNextMessage();
	void sendPong(const QString& msgid);
	void sendClearDirty(const QStringList& categories);
	void sendNode(ProtocolNode* node, bool encrypt = true);
	void sendAck(ProtocolNode* node, const QString& className);
	void sendReceipt(ProtocolNode* node, const QString& type = "read", const QString& participant = QString(), const QString& callId = QString());
	QString createMsgId(const QString& prefix = QString());
	bool processUploadResponse(ProtocolNode* node);
	void handleGroupV2InfoResponse(ProtocolNode *groupNode, bool fromGetGroups = false);

	void setGroupId(const QString& input);
	QString groupId() const;
	QString phoneNumber() const;
	QString challengeFilename() const;
	QString lastId() const;
	QMap<QString, QString>& nodeId() const;
	MessageStoreInterface* messageStore() const;
	QQueue<ProtocolNode*>& messageQueue() const;
	BinTreeNodeWriter* writer() const;
	KeyStream* outputKey() const;
    void getProfilePicture(const QString& number, bool large = false);
    void getLastSeen(const QString& number);
    void getStatus(const QStringList& numbers);
    void setLoginStatus(const QString& input);
    void setLoginTime(const QDateTime& loginTime);
    QString loginStatus() const;
	
protected:	
	QByteArray retrieveChallengeData();
	void processChallenge(const ProtocolNode* node);
	ProtocolNode* createFeaturesNode();
	ProtocolNode* createAuthNode();
	ProtocolNode* createAuthResponseNode();
	
	void sendData(const QByteArray& data);	
	void processInboundData( const QByteArray& data, const bool autoReceipt, const QString& type );
    void processInboundDataNode(ProtocolNode* node, const bool autoReceipt = true, const QString& type = "read");
    QByteArray createAuthBlob();
    QByteArray authenticate();
    bool pollMessage(const bool autoReceipt = true, const QString& type = "read");
    QString getJID( const QString& number );
  //  bool isLoggedIn();
  //  bool doLogin(); 
  //  void processInboundDataNode();

signals:
	void onGetLastSeen(const QString& phoneNumber, const QString& from, const QString& id, const QString& seconds);
	void onStreamError(const QString& tag);
	void onGetError(const QString& phoneNumber, const QString& from, const QString& id, ProtocolNode* node);
	void onProfilePictureChanged(const QString& phoneNumber, const QString& from, const QString& id, const QString& t);
	void onProfilePictureDeleted(const QString& phoneNumber, const QString& from, const QString& id, const QString& t);
    void onGetProfilePicture(const QString& phoneNumber, const QString& from, const QString& type, const QByteArray& data);
	void onNumberWasAdded(const QString& phoneNumber, const QString& jid);
	void onNumberWasRemoved(const QString& phoneNumber, const QString& jid);
	void onNumberWasUpdated(const QString& phoneNumber, const QString& jid);
	void onGetKeysLeft(const QString& phoneNumber, const QString& value);
	void onDisconnect();
	void onPaymentReceived(const QString& phoneNumber, const QString& kind, const QString& status, const QString& creation, const QString& expiration);
	void onCallReceived(const QString& phoneNumber, const QString& from, const QString& id, const QString& notify, const QString& t, const QString& callId);
	void onWebSync(const QString& phoneNumber, const QString& from, const QString& id, const QString& data0, const QString& data1, const QString& data2);
	void onGetFeature(const QString& phoneNumber, const QString& from, const QString& value);
	void onPaidAccount(const QString& phoneNumber, const QString& author, const QString& kind, const QString& status, const QString& creation, const QString& expiration);
	void onGetStatus(const QString& phoneNumber, const QString& jid, const QString& requested, const QString& id, const QString& t, const QString& data);
	void onGetNormalizedJid(const QString& phoneNumber, const QString& result);
	void onGetExtendAccount(const QString& phoneNumber, const QString& kind, const QString& status, const QString& creation, const QString& expiration);
	void onGetServicePricing(const QString& phoneNumber, const QString& price, const QString& cost, const QString& currency, const QString& expiration);
	void onGetBroadcastLists(const QString& phoneNumber, QMap<QString, QMap<QString, QString>>& broadcastlists);
	void onGetGroupV2Info(const QString& phoneNumber, const QString& groupId, const QString& creator, const QString& creation, const QString& subject, const QStringList& participants, const QStringList& admins, bool fromGetGroups);
	void onGroupsParticipantsPromote(const QString& phoneNumber, const QString& from, const QString& t, const QString& participant, const QString& notify, const QStringList& promotedJIds);
	void onGroupsParticipantsRemove(const QString& phoneNumber, const QString& from, const QString& jid);
	void onGroupsParticipantsAdd(const QString& phoneNumber, const QString& from, const QString& jid);
	void onGroupisCreated(const QString& phoneNumber, const QString& creator, const QString& id, const QString& subject, const QString& participant, const QString& creation, const QStringList& groupMembers);
	void onGetGroupsSubject(const QString& phoneNumber, const QString& from, const QString& t, const QString& participant, const QString& notify, const QString& subject);
	void onGetGroups(const QString& phoneNumber, QList<QMap<QString, QString>>& groupList);
	void onGroupsChatCreate(const QString& phoneNumber, const QString& groupId);
	void onGroupsChatEnd(const QString& phoneNumber, const QString& groupId);
	void onMediaUploadFailed(const QString& phoneNumber, const QString& id, ProtocolNode* node, ProtocolNode* messageNode, const QString& description);
	void onGetServerProperties(const QString& phoneNumber, const QString& version, const QMap<QString, QString>& properties);
	void onGetPrivacyBlockedList(const QString& phoneNumber, const QStringList& blockedJids);
	void onGetReceipt(const QString& from, const QString& id, const QString& offline, const QString& retry);
	void onPresenceAvailable(const QString& phoneNumber, const QString& from);
	void onPresenceUnavailable(const QString& phoneNumber, const QString& from, const QString& last);
	void onGetSyncResult( SyncResult* );
	void onPing(const QString& phoneNumber, const QString& id);
	void onSendPong(const QString& phoneNumber, const QString& msgId);
	void onMessagePaused(const QString& phoneNumber, const QString& from, const QString& id, const QString type, const QString& t);
	void onMessageComposing(const QString& phoneNumber, const QString& from, const QString& id, const QString type, const QString& t);
	void onLoginFailure(const QString& phoneNumber, const QString& tag);
	void onLoginSuccess(const QString& phoneNumber, const QString& kind, const QString& status, const QString& creation, const QString& expiration);
	void onAccountExpired(const QString& phoneNumber, const QString& kind, const QString& status, const QString& creation, const QString& expiration);
	void onMessageReceivedServer(const QString& phoneNumber, const QString& from, const QString& id, const QString& classCat, const QString& t);
	void onMessageReceivedClient(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& participant);
	void onGetMessage(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify, const QString& body);
	void onGetGroupMessage(const QString& phoneNumber, const QString& from, const QString& author, const QString& id, const QString& type, const QString& t, const QString& notify, const QString& body);
	void onSendMessageReceived(const QString& phoneNumber, const QString& id, const QString& from, const QString& type);
	void onGetImage(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& size, const QString& url, const QString& file, const QString& mimetype, const QString& filehash, const QString& width, const QString& height,
					const QString& data, const QString& caption);
	void onGetGroupImage(const QString& phoneNumber, const QString& from, const QString& participant, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& size, const QString& url, const QString& file, const QString& mimetype, const QString& filehash, const QString& width, const QString& height,
		const QString& data, const QString& caption);
	void onGetVideo(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& url, const QString& file, const QString& size, const QString& mimetype, const QString& filehash, const QString& duration, const QString& vcodec,
		const QString& acodec, const QString& data, const QString& caption);
	void onGetGroupVideo(const QString& phoneNumber, const QString& from, const QString& participant, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& url, const QString& file, const QString& size, const QString& mimetype, const QString& filehash, const QString& duration, const QString& vcodec,
		const QString& acodec, const QString& data, const QString& caption);
	void onGetAudio(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& size, const QString& url, const QString& file, const QString& mimetype, const QString& filehash, const QString& seconds, const QString& acodec, const QString& author);
	void onGetvCard(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& name, const QString& data, const QString& author);
	void onGetLocation(const QString& phoneNumber, const QString& from, const QString& id, const QString& type, const QString& t, const QString& notify,
		const QString& name, const QString& longitude, const QString& latitude, const QString& url, const QString& data, const QString& author);
	
private:
	Q_INVOKABLE void onConnected();
	Q_INVOKABLE void onDisconnected();
	Q_INVOKABLE void onBytesWritten(qint64);
	Q_INVOKABLE void onReadyRead();
	Q_INVOKABLE void onError(QAbstractSocket::SocketError error);
    //QByteArray readStanza();
    //Q_INVOKABLE void pollMessage();

    WhatsAppControllerPrivate *d;
};
}//namespace whatsapptracker

#endif
