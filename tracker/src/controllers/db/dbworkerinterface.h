#ifndef WHATSAPPTRACKER_DBWORKERINTERFACE_H
#define WHATSAPPTRACKER_DBWORKERINTERFACE_H

#include <QObject>
#include <QList>
#include "../../models/user.h"
#include "../../models/config.h"
#include "../../models/trackednumber.h"

namespace whatsapptracker {

class DbWorkerInterface : public QObject
{
    Q_OBJECT

public:
    DbWorkerInterface(QObject *parent = NULL) : QObject(parent){}
    virtual void connect(const QString& url, const QString& dbName, const QString& username, const QString& password)=NULL;
   
public slots:
    //tracked numbers management
    virtual void insertTrackedNumber(const TrackedNumber& TrackedNumber) = NULL;
    virtual void updateTrackedNumber(const TrackedNumber& TrackedNumber) = NULL;
    virtual void deleteTrackedNumber(const QString& phoneNumber) = NULL;
    virtual void getTrackedNumber(const QString& phoneNumber) = NULL;

    //users management
    virtual void getUsers() = NULL;
    virtual void updateUser(const User& users) = NULL;

    //config management
    virtual void getConfig() = NULL;
    virtual void updateConfig(const Config& config) = NULL;

signals:
    //tracked numbers management
    void onInsertTrackedNumber(const QString& phoneNumber, const bool status);
    void onUpdateTrackedNumber(const QString& phoneNumber, const bool status);
    void deleteTrackedNumber(const QString& phoneNumber, const bool status);
    void getTrackedNumber(const TrackedNumber& TrackedNumber);

    //users management
    void onGetUsers(const QList<User>& user);
    void onUpdateUser(const bool status);

    //config management
    void onGetConfig(const Config& config);
    void onUpdateConfig(const bool status);
};
}
#endif